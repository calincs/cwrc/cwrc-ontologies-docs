module.exports = {
    env: {
        es2021: true,
        node: true,
    },

    parserOptions: {
        ecmaVersion: 'latest',
        SourceType: 'module',
    },

    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@docusaurus/all',
        'plugin:react-hooks/recommended',
        'plugin:regexp/recommended',
        'prettier',
    ],

    rules: {
        'no-duplicate-imports': 'warn',
        'no-self-compare': 'warn', // Not important but does not hurt to include
        'no-var-requires': 'off',
        '@docusaurus/no-untranslated-text': 'off',

        'comma-spacing': 'off',
        '@typescript-eslint/comma-spacing': 'warn',
        '@typescript-eslint/ban-types': 'error',
        'no-use-before-define': 'off',
        '@typescript-eslint/no-use-before-define': 'error', //Adds support for 'type', 'interface', and 'enum' declarations
    },

    plugins: ['react-hooks', '@typescript-eslint', '@docusaurus', 'prettier'],
};
