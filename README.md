# The CWRC Linked Data Website

This website is built using [Docusaurus 3](https://docusaurus.io/), a modern static website generator that uses [MDX](https://mdxjs.com/).

The site is deployed to **TBD**.

## Installation

You will need Git (with an ssh key set up to connect to GitLab), NodeJS (v18 or newer), and a code editor (VS Code is recommended)  to develop this website.

```bash
git clone git@gitlab.com:calincs/cwrc/cwrc-ontologies-docs.git
cd cwrc-ontologies-docs
```

## Local development

```bash
npm install
npm start
```

This command starts a local development server at `http://localhost:3000/` and opens up a browser window. Most changes to `src/` are reflected live without having to restart the server, but for most changes to markdown files, the server will have restarted.

After all your changes are reflected on `http://localhost:3000/` it's a good idea to test the build.

1. Ctrl-C to stop your current local development server
2. `npm run build` - This does a full rebuild of the site
3. `npm run serve` - This starts local development server at `http://localhost:3000/`
4. Review any errors that may have shown up and revise. You can also test by using the docker compose file if you have docker set up.

This is good to do before committing to ensure a broken build isn't pushed to GitLab.

### RSDoctor Script

```bash
npm run build:rsdoctor
```

The command will begin the build process with the RSDoctor plugin enabled. Once the build has been processed you will find two links that will take you to the analysis from RSDoctor, a potential example being http://localhost:4248/index.html. 

RSDoctor will provide a detailed analysis on the overall project and bundle compile time, along with the warnings and errors provided from the base 'npm run build command. This should only be used in development. 

### View Translation Locally

To see the converted ISO locally at `http://localhost:3000/`, run the following command in your Docusaurus repo:

```bash
npm run start -- --locale your_iso_code
```

If you want to be able to see both the English version and your ISO code translations, run `npm run build` and then `npm run serve`.

The translations are not always perfect, please double check your files have the correct translation after conversion before committing your changes.

### Making Edits

Prior to making any edits for the first time, you must review the [Project Documentation Guide](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/home), particularly the [Accessibility](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/accessibility) & [Style](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/style-guide) Guide for guidelines on writing documentation.

See the [Maintenance Guide](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/maintenance-guide) for instructions on how to get started making edits.

Filename extensions must `mdx` if using any custom components. (Generally recommended to use mdx in case you decide to later add buttons or other things)

## Plugins Used

- @docusaurus/plugin-client-redirects - used to handle a few redirects from old site
- glossary directive - custom glossary plugin using Text Directives 

## Developer Practices

### Recommended Resources

- [Docusaurus Guides](https://docusaurus.io/docs/category/guides)
  - [Styling & Layout](https://docusaurus.io/docs/styling-layout)
  - [MDX & React](https://docusaurus.io/docs/markdown-features/react)
  - [Static Assets](https://docusaurus.io/docs/static-assets)
  - [Translate your React Code](https://docusaurus.io/docs/i18n/tutorial#translate-your-react-code)
- [Docusaurus's Source code for their Docusaurus Site](https://github.com/facebook/docusaurus/tree/main/website)
- [MDN CSS Guide](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks)
- [LINCS Wiki - Accessibility Guide](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/accessibility)

### Best Practices

- This project uses CSS modules to organize its CSS, see <https://docusaurus.io/docs/styling-layout#css-modules>
  - When styling a custom component, look at the implementation for the component to find the particular CSS class that should be used
- Code should be formatted using Prettier settings found in package.json
- Avoid absolute styling when possible, have responsiveness in mind.
  - Remember to keep translation in mind, French words tend to be longer
  - We also have French assets that should be used where available on French pages
- After creating new components that will be used in multiple places, document expected usage in [Snippets](https://gitlab.com/calincs/admin/lincsaurus/-/snippets)

## Rebuilding the search index

We use a _Typesense_ index for searching the CWRC Linked Data site. The index is hosted on LINCS infrastructure and is updated with the CI pipeline. If you want to host an index locally, you'd need to rebuild it manually if we wish to update it.

Start the Typesense server:

```bash
./typesense.sh
```

Then run the scraper in a new terminal, which will index `https://portal.lincsproject.ca` with the current configuration and write the index to your local typesense server:

```bash
./scraper.sh
```

## Redirects

The following urls are being redirected:
...

