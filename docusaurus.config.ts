import { themes as prismThemes } from 'prism-react-renderer';
import type { Config } from '@docusaurus/types';
import type * as Preset from '@docusaurus/preset-classic';

require('dotenv').config();


const config: Config = {
    title: 'CWRC Linked Data',
    tagline: 'Learn about the CWRC Ontologies',
    favicon: 'img/favicon.ico',

    // Set the production url of your site here
    url: 'https://your-docusaurus-site.example.com',
    // Set the /<baseUrl>/ pathname under which your site is served
    // For GitHub pages deployment, it is often '/<projectName>/'
    baseUrl: '/',

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: 'CWRC', // Usually your GitHub org/user name.
    projectName: 'cwrc-ontologies-docs', // Usually your repo name.

    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',

    // Even if you don't use internationalization, you can use this field to set
    // useful metadata like html lang. For example, if your site is Chinese, you
    // may want to replace "en" with "zh-Hans".
    i18n: {
        defaultLocale: 'en',
        locales: ['en', 'fr'],
        localeConfigs: {
            en: {
                label: 'English',
            },
            fr: {
                label: 'Français',
            },
        },
    },

    future: {
        experimental_faster: true,
    },

    markdown: {
        mermaid: true,
    },

    themes: ['@docusaurus/theme-mermaid', 'docusaurus-theme-search-typesense'],

    plugins: [...(process.env.RSDOCTOR === 'true' ? ['rsdoctor'] : [])],

    presets: [
        [
            'classic',
            {
                docs: {
                    sidebarPath: './sidebars.ts',
                    // Please change this to your repo.
                    // Remove this to remove the "edit this page" links.
                    sidebarCollapsed: false,

                    editUrl:
                        'https://gitlab.com/calincs/cwrc/cwrc-ontologies-docs/-/tree/main/',
                },
                blog: false,
                theme: {
                    customCss: './src/css/custom.css',
                },
            } satisfies Preset.Options,
        ],
    ],

    themeConfig: {
        // Replace with your project's social card
        colorMode: {
            defaultMode: 'dark',
            disableSwitch: false,
            respectPrefersColorScheme: false,
        },
        image: 'img/docusaurus-social-card.jpg',
        navbar: {
            title: 'CWRC Linked Data',
            logo: {
                alt: 'My Site Logo',
                src: 'img/cwrc-logo-vertical-white.png',
            },
            items: [
                // {
                //   type: "docSidebar",
                //   sidebarId: "tutorialSidebar",
                //   position: "left",
                //   label: "Tutorial",
                // },
                {
                    type: 'docSidebar',
                    sidebarId: 'sparqlSidebar',
                    position: 'left',
                    label: 'SPARQL Queries',
                },
                // {
                //   type: "docSidebar",
                //   sidebarId: "ontologiesSidebar",
                //   position: "left",
                //   label: "Ontologies",
                // },
                {
                    type: 'docSidebar',
                    sidebarId: 'ontologiesSidebar',
                    label: 'Ontologies',
                    position: 'left',
                },

                {
                    type: 'docSidebar',
                    sidebarId: 'aboutSidebar',
                    position: 'left',
                    label: 'About',
                },
                {
                    href: 'https://lincsproject.ca',
                    label: 'LINCS',
                    position: 'right',
                },
                {
                    type: 'localeDropdown',
                    position: 'right',
                    className: 'navbar-item',
                },
            ],
        },
        footer: {
            style: 'dark',
            links: [
                {
                    title: 'Contact Us',
                    items: [
                        {
                            label: 'Feedback',
                            to: '#',
                        },
                        {
                            label: 'Email Us',
                            to: '#',
                        },
                    ],
                },
                {
                    title: 'About Us',
                    items: [
                        {
                            label: 'People',
                            href: '/docs/about/people/',
                        },
                        {
                            label: 'Funders',
                            href: '/docs/about/funders/',
                        },
                    ],
                },
            ],
            copyright: ` <div class="land-acknowledgement">
                  <p>While members of CWRC gather together on-line from all across North America and beyond, our servers, which are also on Indigenous territories, produce the worlds we create and meet in. In particular we want to acknowledge that our servers are situated on the traditional lands of the WSÁNEĆ (Saanich), Lkwungen (Songhees), and Wyomilth (Esquimalt) peoples.</p>
                </div><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0;margin-right:2px;height:calc(1em - 4px);" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a>This work is licensed under a <a rel="license" class="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.`,
        },
        prism: {
            theme: prismThemes.github,
            darkTheme: prismThemes.dracula,
            additionalLanguages: ['turtle', 'sparql'],
        },
        typesense: {
            typesenseCollectionName: 'cwrc-ontologies',
            typesenseServerConfig: {
                nodes: [
                    {
                        host: process.env.REACT_APP_HOST,
                        port: process.env.REACT_APP_PORT,
                        protocol: process.env.REACT_APP_PROTOCOL,
                    },
                ],
                apiKey: process.env.REACT_APP_API_KEY,
            },
            // Optional: Typesense search parameters: https://typesense.org/docs/0.21.0/api/documents.md#search-parameters
            typesenseSearchParameters: {},
            // Optional
            contextualSearch: true,
        },
    } satisfies Preset.ThemeConfig,
};

export default config;
