export interface CardNavigationInterface {
    name: string;
    link: string;
}

export interface OntologyCardInterface {
    id: number;
    name: JSX.Element;
    shortDescription: JSX.Element;
    overviewLink: string;
    introductionLink: string;
    documentationLink: string;
}

export interface Datasets {
    [key: string]: OntologyCardInterface;
}
