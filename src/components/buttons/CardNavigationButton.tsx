import React from 'react';
import styles from './button.module.css';
import Link from '@docusaurus/Link';
import { CardNavigationInterface } from '@site/src/types';

const cardNavigationButton = ({ name, link }: CardNavigationInterface) => {
    return (
        <>
            <Link className={`${styles.navigationButton}`} to={link}>
                {name}
            </Link>
        </>
    );
};
export default cardNavigationButton;
