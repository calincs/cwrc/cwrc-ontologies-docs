/** @format */

import React from 'react';
import styles from './cards.module.css';
import { translate } from '@docusaurus/Translate';
import NavButton from '../buttons/CardNavigationButton';
import Heading from '@theme/Heading';
import { OntologyCardInterface } from '@site/src/types';
const OntologyCard = ({
    name,
    shortDescription,
    overviewLink,
    introductionLink,
    documentationLink,
}: OntologyCardInterface) => {
    return (
        <div className={`${styles['ontologyCardContainer']}`}>
            <div className={styles.cardTitle}>
                <Heading as="h2"> {name} </Heading>
            </div>
            <div className={styles.cardDescription}>{shortDescription}</div>
            <div className={styles.cardButtonContainer}>
                <NavButton
                    link={overviewLink}
                    name={translate({
                        message: 'Overview',
                        id: 'ontology.overview.button',
                        description: 'Button Text to Ontology Overview',
                    })}
                />
                <NavButton
                    link={documentationLink}
                    name={translate({
                        message: 'Terms & Definitions',
                        id: 'ontology.documentation.button',
                        description: 'Button Text to Ontology Documentation',
                    })}
                />
                <NavButton
                    link={introductionLink}
                    name={translate({
                        message: 'Introduction',
                        id: 'ontology.introduction.button',
                        description: 'Button Text to Ontology Introduction',
                    })}
                />
            </div>
        </div>
    );
};

export default OntologyCard;
