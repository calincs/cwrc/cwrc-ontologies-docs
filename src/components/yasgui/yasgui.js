/** @format */

import '@triply/yasgui/build/yasgui.min.css';
import React, { useState, useEffect } from 'react';
import useIsBrowser from '@docusaurus/useIsBrowser';

const config = {
    yasqe: {
        persistent: false,
        value: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>`,
        endpoint: 'https://fuseki.lincsproject.ca/cwrc/sparql', // Set the default endpoint here
    },
    yasr: {
        plugins: {
            prefixes: {
                rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
                owl: 'http://www.w3.org/2002/07/owl#',
                ii: 'http://sparql.cwrc.ca/ontology/ii#',
                cwrc: 'http://sparql.cwrc.ca/ontology/cwrc#',
                genre: 'http://sparql.cwrc.ca/ontology/genre#',
            },
        },
    },
    catalog: {
        endpoints: [
            {
                endpoint: 'https://fuseki.lincsproject.ca/cwrc/sparql',
                title: 'CWRC',
            },
            {
                endpoint: 'https://fuseki.lincsproject.ca/lincs/sparql',
                title: 'LINCS',
            },
            {
                endpoint: 'https://dbpedia.org/sparql',
                title: 'DBpedia',
            },
            {
                endpoint: 'https://query.wikidata.org/sparql',
                title: 'Wikidata',
            },
        ],
    },
};

export default function App() {
    const isBrowser = useIsBrowser();

    useEffect(() => {
        if (isBrowser) {
            import('@triply/yasgui').then(({ default: Yasgui }) => {
                const yasgui = new Yasgui(document.getElementById('yasgui'), {
                    requestConfig: {
                        endpoint: 'https://fuseki.lincsproject.ca/cwrc/sparql',
                    },
                    copyEndpointOnNewTab: true,
                    resizeable: true,
                    autofocus: true,
                    yasqe: {
                        value: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX cwrc: <http://sparql.cwrc.ca/ontology/cwrc#>
PREFIX genre: <http://sparql.cwrc.ca/ontology/genre#>
PREFIX ii: <http://sparql.cwrc.ca/ontology/ii#>

SELECT * WHERE {
  ?sub ?pred ?obj .
} LIMIT 10

`,
                    },
                    yasr: {
                        plugins: {
                            prefixes: {
                                rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                                rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
                                owl: 'http://www.w3.org/2002/07/owl#',
                                ii: 'http://sparql.cwrc.ca/ontology/ii#',
                                cwrc: 'http://sparql.cwrc.ca/ontology/cwrc#',
                                genre: 'http://sparql.cwrc.ca/ontology/genre#',
                            },
                        },
                    },
                    endpointCatalogueOptions: {
                        getData: () => {
                            return [
                                //List of objects should contain the endpoint field
                                //Feel free to include any other fields (e.g. a description or icon
                                //that you'd like to use when rendering)
                                {
                                    title: 'CWRC',
                                    endpoint:
                                        'https://fuseki.lincsproject.ca/cwrc/sparql',
                                    description:
                                        'The Canadian Writing Research Collaboratory',
                                },
                                {
                                    title: 'LINCS',
                                    endpoint:
                                        'https://fuseki.lincsproject.ca/lincs/sparql',
                                },
                                {
                                    title: 'LINCS Vocabularies',
                                    endpoint:
                                        'https://fuseki.lincsproject.ca/skosmos/sparql',
                                },
                                {
                                    title: 'DBpedia',
                                    endpoint: 'https://dbpedia.org/sparql',
                                },
                                {
                                    title: 'Wikidata',
                                    endpoint: 'https://query.wikidata.org',
                                },
                                // ...
                            ];
                        },
                        keys: ['title'],
                    },
                });
            });
        }
        return () => {};
    }, [isBrowser]);

    return <div id="yasgui" />;
}
