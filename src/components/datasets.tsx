import { Datasets } from '../types';
import Translate from "@docusaurus/Translate";

const ontologyCardData: Datasets = {
    cwrc: {
        id: 1,
        name: (
            <Translate
                id="ontology.cwrc.title"
                description="Title for CWRC Ontology Card"> 
                CWRC Ontology
            </Translate> ),
        shortDescription: (
            <Translate
                id="ontology.cwrc.shortDescription"
                description="short description for CWRC Ontology">
                The Ontology of the Canadian Writing Research Collaboratory
                brings together various linked data materials produced within
                the Collaboratory related to the writers, writing, and culture.
            </Translate> ),
        overviewLink: '/docs/ontologies/cwrc-ontology/',
        introductionLink: '/docs/ontologies/cwrc-ontology/cwrc-introduction',
        documentationLink:
            '/docs/ontologies/cwrc-ontology/cwrc-terms-definitions',
    },

    genre: {
        id: 2,
        name: (
        <Translate
            id="ontology.genre.title"
            description="Title for CWRC Ontology Card"> 
            Genre Ontology
        </Translate> ),
        shortDescription: (
        <Translate
            id="ontology.genre.shortDescription"
            description="short description for Genre Ontology"> 
            The CWRC Genre Ontology was developed by the Canadian Writing Research Collaboratory to identify genres 
            associated with various forms of cultural production, particularly literary production.
        </Translate> ),
        overviewLink: '/docs/ontologies/genre-ontology/',
        introductionLink: '/docs/ontologies/genre-ontology/genre-introduction',
        documentationLink:
            '/docs/ontologies/genre-ontology/genre-terms-definitions',
    },

    II: {
        id: 3,
        name: (
            <Translate
                id="ontology.ii.title"
                description="Title for II Ontology Card"> 
                II Ontology
            </Translate> ),
        shortDescription: (
        <Translate
            id="ontology.ii.shortDescription"
            description="short description for II Ontology">
            The II (Illness and Injury) Ontology is the ontology used by the Canadian Writing Research Collaboratory to 
            assign names of illnesses and injuries to cause of death and health issues triples.
        </Translate> ),
        overviewLink: '/docs/ontologies/ii-ontology/',
        introductionLink: '/docs/ontologies/ii-ontology/ii-introduction',
        documentationLink: '/docs/ontologies/ii-ontology/ii-terms-definitions',
    },
};

export default ontologyCardData;
