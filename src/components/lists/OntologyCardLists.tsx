import React from 'react';
import datasets from '../datasets';
import styles from '../lists/lists.module.css';
import OntologyCard from '../cards/OntologyCard';
import { Datasets, OntologyCardInterface } from '../../types';

const requiredProps = [
    'name',
    'shortDescription',
    'overviewLink',
    'introductionLink',
    'documentationLink',
];

function setProperties(datasets: Datasets, index: string): number {
    const tempString: number = datasets[index as keyof Datasets]?.id ?? 0;

    return tempString;
}

// Determine if each Ontology Card has the correct amount of items.
function setProps(dataset: OntologyCardInterface): boolean {
    if (!dataset || !requiredProps.every((prop) => prop in dataset)) {
        const missingProps = requiredProps.filter((prop) => !(prop in dataset));
        console.error(
            'Missing parameter for Ontology Card. Ensure the data has the following keys: ',
            requiredProps,
            'Missing Parameter(s): ',
            missingProps,
        );
        console.error('Ontology Card that failed: ', dataset);
        return false;
    }

    return true;
}

const data = Object.keys(datasets).sort((a, b) => {
    const propertyA = setProperties(datasets, a);
    const propertyB = setProperties(datasets, b);

    if (propertyA == 0 || propertyB == 0) {
        return 0;
    }
    // Compare the properties for sorting
    return propertyA - propertyB;
});

const OntologyCardLists = () => {
    return (
        <div className={styles.cardContainer}>
            {data.map((i) => {
                const dataset = datasets[i]
                if ((!dataset) || !setProps(datasets[i]!)) {
                    return null;
                } else {

                    const {
                        id = 0,
                        name = <div></div>,
                        shortDescription = <div></div>,
                        overviewLink = '',
                        introductionLink = '',
                        documentationLink = '',
                    } = dataset
                    
                    return (
                        <OntologyCard
                            key={i}
                            id={id}
                            name={name}
                            shortDescription={shortDescription}
                            overviewLink={overviewLink}
                            introductionLink={introductionLink}
                            documentationLink={documentationLink}
                        />
                    );
                }
            })}
        </div>
    );
};

export default OntologyCardLists;
