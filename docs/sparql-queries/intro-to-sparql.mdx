---
sidebar_position: 2
title: "Intro to SPARQL"
---

# An Introduction to SPARQL Queries

## What is SPARQL?

SPARQL (SPARQL Protocol and RDF Query Language, pronounced “sparkle”) is a query language used to retrieve and manipulate linked data stored in an RDF format. In this format, data are stored as triples. Triples are formed by a subject, predicate, and object. Together they form an assertion that the subject has a particular property or relationship, indicated by the predicate, to the object.

## Components of a Query

The best way to get started is to write a simple query.

Example: **[This query](https://yasgui.lincsproject.ca/#query=Select+%3Fsubject+%3Fpredicate+%3Fobject+WHERE+%7B%0A++%3Fsubject+%3Fpredicate+%3Fobject+.%0A%7D%0ALIMIT+10&contentTypeConstruct=text%2Fturtle&contentTypeSelect=application%2Fsparql-results%2Bjson&endpoint=https://fuseki.lincsproject.ca/cwrc/sparql&requestMethod=POST&tabTitle=First+Ten+Triples&headers=%7B%7D&outputFormat=table)** will return the first ten triples in the dataset:

```sparql
Select ?subject ?predicate ?object WHERE {
  ?subject ?predicate ?object .
}
LIMIT 10
```

Let's break down this query further.

**?:**

Any word following a question mark is a variable. In this query **subject**, **predicate**, and **object** are names of variables. These variable names can be changed so long as they are from the ontology of the database you are querying.

**SELECT:**

The variables following SELECT determine what will be displayed in the table of results. For this query the table of results will have a column each of subjects, predicates, and objects. Other keywords such as CONSTRUCT, ASK, and DESCRIBE can be used to display other results.

**WHERE:**

The WHERE clause is mandatory. Within the WHERE clause are triples that specify the conditions the triples in the dataset must meet to be included in the results. In this query, we are searching the dataset for triples that have **subject**, **predicate**, and **object**.

**LIMIT:**

The keyword LIMIT is an example of a query modifier. This keyword will restrict the results so that only the first 10 are displayed. There are many other keywords and commands that will modify query results, which are explored in more depth in **[Query Modifiers](query-modifiers)**.

## Prefixes and URIs

Each part of a triple (subject, predicate, object) is identified by a **[Uniform Resource Identifier (URI)](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier)**.

For example, the gender property **["woman"](http://sparql.cwrc.ca/ontologies/cwrc#woman)** in the CWRC Ontology is represented by the following URI: **[&lt;http://sparql.cwrc.ca/ontologies/cwrc#woman>](http://sparql.cwrc.ca/ontologies/cwrc#woman)**

URIs in queries are often shortened using a **prefix**, or **[namespace](https://en.wikipedia.org/wiki/Namespace)**, which is defined at the beginning of the query. These stand in for most of the URI, leaving the variable at the end which designates the terms from that ontology or vocabulary.

Defining a prefix at the beginning of a query uses the keyword PREFIX, the namespace, and the URI of the ontology. For example:

```sparql
PREFIX cwrc: <http://sparql.cwrc.ca/ontologies/cwrc#>
```

Thus the same term above could be represented as: **cwrc:woman**

## A Simple Query

```sparql
PREFIX cwrc: <http://sparql.cwrc.ca/ontologies/cwrc#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
SELECT ?name WHERE {
    ?person cwrc:hasGender cwrc:woman .
    ?person cwrc:hasOccupation cwrc:teacher .
    ?person skos:altLabel ?name
}
```

**[This query](https://yasgui.lincsproject.ca/#query=PREFIX+cwrc%3A+%3Chttp%3A%2F%2Fsparql.cwrc.ca%2Fontologies%2Fcwrc%23%3E%0APREFIX+skos%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0ASELECT+%3Fname+WHERE+%7B+%0A++++%3Fperson+cwrc%3AhasGender+cwrc%3Awoman+.+%0A++++%3Fperson+cwrc%3AhasOccupation+cwrc%3Ateacher+.%0A++%09%3Fperson+skos%3AaltLabel+%3Fname%0A%7D&contentTypeConstruct=text%2Fturtle&contentTypeSelect=application%2Fsparql-results%2Bjson&endpoint=https://fuseki.lincsproject.ca/cwrc/sparql&requestMethod=POST&tabTitle=Teachers+who+were+women&headers=%7B%7D&outputFormat=table)** searches for all of the triples in the CWRC dataset that satisfy the following requirements: the person is a woman and a teacher. We can break down the pieces of this query.

The namespaces used here are **cwrc** and **skos**:

```sparql
PREFIX cwrc: <http://sparql.cwrc.ca/ontologies/cwrc#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
```

The results will be displayed in a table that has one column, **name**:

```sparql
SELECT ?name
```

The WHERE clause is searching for a person who is a woman and a teacher:

```sparql
WHERE {
  ?person cwrc:hasGender cwrc:woman .
  ?person cwrc:hasOccupation cwrc:teacher .
  ?person skos:altLabel ?name
}
```

In plain English, it could be translated as follows:

_There is a person who has the gender woman_

```sparql
?person cwrc:hasGender cwrc:woman .
```

_That same person has an occupation of teacher_

```sparql
?person cwrc:hasOccupation cwrc:teacher .
```

_That person has a name represented by an alt label_

```sparql
?person skos:altLabel ?name
```

## Notes on Style

Every triple in a query should end in a period.

If a subject used in one line is repeated in the next line it can be omitted as long as there is a semicolon at the end to indicate use of the same subject.

**[Example:](<https://yasgui.lincsproject.ca/#query=PREFIX+cwrc%3A+%3Chttp%3A%2F%2Fsparql.cwrc.ca%2Fontologies%2Fcwrc%23%3E%0APREFIX+skos%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0ASELECT+%3Fname+WHERE+%7B+%0A++++%3Fperson+cwrc%3AhasGender+cwrc%3Awoman%3B+%0A++++%09%09cwrc%3AhasOccupation+cwrc%3Ateacher%3B%0A++++++%09%09skos%3AaltLabel+%3Fname.%0A%7D&contentTypeConstruct=text%2Fturtle&contentTypeSelect=application%2Fsparql-results%2Bjson&endpoint=https://fuseki.lincsproject.ca/cwrc/sparql&requestMethod=POST&tabTitle=Teachers+who+were+women+(%3B+notation)&headers=%7B%7D&outputFormat=table>)**

```sparql
PREFIX cwrc: <http://sparql.cwrc.ca/ontologies/cwrc#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
SELECT ?name WHERE {
    ?person cwrc:hasGender cwrc:woman;
            cwrc:hasOccupation cwrc:teacher;
            skos:altLabel ?name.
}
```
