---
sidebar_position: 3
title: "Query Modifiers"
---

# Query modifiers: Refining Your Search

SPARQL has many built-in keywords which can be used to refine your search. The table below provides a list of some of the main keywords along with a brief description.

## About Query Modifiers

There are many more modifiers than those listed in the table below. More information about them can be found on the **[W3 Schools SPARQL Documentation](https://www.w3.org/TR/rdf-sparql-query/)**, or in the **[SPARQL WikiBooks](https://en.wikibooks.org/wiki/SPARQL)**.

Many modifiers can take a number of different formats of parameters, such as regex (regular expression). More information about writing regex can be found in the **[SPARQL Wikibooks](https://en.wikibooks.org/wiki/SPARQL/Expressions_and_Functions#REGEX)**, and on the **[W3 Schools website](https://www.w3.org/TR/xpath-functions/#regex-syntax)**. Another website to help get used to writing regex is **[Regex Explained](https://leaverou.github.io/regexplained/)**.

Often data types need to be specified when using specific values as parameters for modifiers. There are a number of different datatypes, such as xsd:integer (a whole number), xsd:boolean (data that is either true or false), or xsd:decimal (a number with values after the decimal point). To specify these datatypes, use the format ^^xsd:datatype.

For example, if we wanted to find people with dates of birth between Jan. 1 and Dec. 31, 1884, we would use a regex that specified that those values were dates:

```sparql
FILTER ((?birthDate >= '1844-01-01'^^xsd:date) && (?birthDate <= '1884-12-31' ^^xsd:date))
```

More information about the basic datatypes can be found on the **[W3 Schools XML Schema Pages](https://www.w3.org/TR/xmlschema-2/#built-in-primitive-datatypes)**.

## Query Modifiers

<table>
  <tr>
   <td><strong>Keyword</strong></td>
   <td><strong>Purpose</strong></td>
   <td><strong>Example</strong></td>
  </tr>
  <tr>
   <td>DISTINCT</td>
   <td>Prevents duplicate results to be returned.</td>
   <td>
   ```sparql
   SELECT DISTINCT ?birthdate ?name WHERE { 
       ?person cwrc:hasGender cwrc:woman;
           cwrc:hasBirthDate ?birthdate;
               skos:altLabel ?name.
   }
   ```
   <p> **[This Query](https://yasgui.lincsproject.ca/#)** returns the names and birthdays of women with no duplicate entries. </p>
   </td>
   </tr>

   <tr>
   <td>BIND</td>
   <td>Assigns the result of an expression to a variable, using the format **"BIND(expression AS ?variable).**"</td>
   <td>
   ```sparql
    SELECT ?name ?birthDate ?deathDate ?age WHERE { 
        ?person rdfs:label ?name; 
            cwrc:hasDeathDate ?deathDate; 
            cwrc:hasBirthDate ?birthDate. 
        BIND(?deathDate - ?birthDate as ?ageInDays). 
        BIND(?ageInDays/365.2425 AS ?ageInYears). 
        BIND(FLOOR(?ageInYears) AS ?age). 
    } 
   ```
   <p> **[This Query](https://yasgui.lincsproject.ca/#)** returns the names and ages of people in the database. The age is calculated using the BIND keyword to assign values to variables such as date of birth, date of death, and ageInDays, so operations can be performed on them without overwriting the variable each time. </p>
   </td>
   </tr>

   <tr>
   <td>FILTER</td>
   <td>	Filters results by a provided clause. You can filter by using a regex (regular expression) that will return true/false, a range of dates, the language of a label, and more.</td>
   <td>
   ```sparql
   SELECT ?name ?birthDate WHERE { 
       ?person cwrc:hasBirthDate ?birthDate ; 
           cwrc:hasGender cwrc:woman; 
           rdfs:label ?name. 
    FILTER ((?birthDate >= '1900-01-01'^^xsd:date)) .
   }
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)** returns names of women with birthdates after January 1, 1900.</p>
   </td>
   </tr>

   <tr>
   <td>FILTER NOT EXIST</td>
   <td>Filters results, returning results which do not meet the provided clause.</td>
   <td>
   ```sparql
    SELECT ?name WHERE { 
        ?person cwrc:hasBirthDate ?birthDate ; 
            cwrc:hasGender cwrc:woman; 
            rdfs:label ?name. 
        FILTER NOT EXISTS { 
            ?person cwrc:hasBirthDate "1775-12-16"^^xsd:date 
        } 
    } 
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)** returns names of women except for those with the birthday December 16, 1776 (Jane Austen).  </p>
   </td>
   </tr>

   <tr>
   <td>GRAPH</td>
   <td>Specifies which graph data will be retrieved from.</td>
   <td>
   ```sparql
    SELECT ?obj ?snippet WHERE { 
        GRAPH  { 
            ?context ?pred ?obj; 
                cwrc:contextFocus ?person; 
                    oa:hasTarget ?target;
                rdf:type cwrc:DeathContext.
            ?person rdfs:label "Donne, John".
    }.
        ?target oa:hasSelector/oa:refinedBy/oa:exact ?snippet. 
    } 
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)** will return snippets relating to John Donne's death. It does so by using GRAPH to get the context from the BiographyV2Beta database where it is stored.</p>
   </td>
   </tr>

   <tr>
   <td>GROUP BY</td>
   <td>Groups the results together according to specified criteria. After grouping results, you can then perform functions on these groups, such as COUNT(), AVG(), SUM(), MIN(), and MAX(). More information on aggregate functions can be found **[here](https://en.wikibooks.org/wiki/SPARQL/Aggregate_functions)**.</td>
   <td>
   ```sparql
    SELECT ?occupation (COUNT(?person) AS ?numPerProf) WHERE { 
        ?person cwrc:hasGender cwrc:woman;
            cwrc:hasOccupation ?occupation. 
    }
    GROUP BY ?occupation
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)**  returns occupations of women in CWRC. The query first finds all the women with occupations, groups them by their occupations (using GROUP BY), and then counts the number of people in each group. These counts will be displayed in the results table in a column labeled "numPerProf", the number of people in each profession.</p>
   </td>
   </tr>

   <tr>
   <td>LIMIT</td>
   <td>Limits the number of results returned. The results will have a random order unless using the 'ORDER BY' keyword.</td>
   <td>
   ```sparql
    SELECT ?name WHERE { 
        ?person cwrc:hasGender cwrc:woman .
        ?person rdfs:label ?name .
    }LIMIT 10
   ```
   <p> **[This Query](https://yasgui.lincsproject.ca/#)** returns the first ten names of women. </p>
   </td>
   </tr>

   <tr>
   <td>MINUS</td>
   <td>Similar to FILTER NOT EXISTS; can be used to prevent certain objects from being included in the results.</td>
   <td>
   ```sparql
    SELECT ?name WHERE {
        ?person cwrc:hasGender cwrc:woman .
        ?person rdfs:label ?name .
        MINUS {
            ?person rdfs:label "Askew, Anne".
        }.
    } 
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)** returns names of women, except those named "Anne Askew".</p>
   </td>
   </tr>

   <tr>
   <td>OFFSET</td>
   <td>Skips the first number of rows specified by offset.</td>
   <td>
   ```sparql
    SELECT ?name WHERE { 
        ?person cwrc:hasGender cwrc:woman .
        ?person rdfs:label ?name .
 
    }OFFSET 10 
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)** returns names of women, starting at the eleventh name.</p>
   </td>
   </tr>

   <tr>
   <td>OPTIONAL</td>
   <td>Makes the specified criteria optional. If the object exists, it will be displayed in the results, but if not, the cell in the results will simply be empty.</td>
   <td>
   ```sparql
    SELECT ?name ?deathDate WHERE {
        ?person cwrc:hasGender cwrc:woman;
        rdfs:label ?name .
        OPTIONAL {?person cwrc:hasDeathDate ?deathDate}.
    }    
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)** returns names of women and their date of death. If they do not have a date of death, their name will still be displayed, but the column for date of death will be empty for that row. </p>
   </td>
   </tr>

   <tr>
   <td>ORDER BY</td>
   <td>	Sorts the data by the values specified. Default is ascending order, unless otherwise specified.</td>
   <td>
   ```sparql
    SELECT ?name ?birthDate WHERE {
        ?person cwrc:hasBirthDate ?birthDate.
        ?person rdfs:label ?name .
 
    }ORDER BY ?birthDate 
   ```
   <p>**[This Query](https://yasgui.lincsproject.ca/#)** returns names of people, ordered by their date of birth.</p>
   </td>
   </tr>

   <tr>
   <td>VALUES</td>
   <td>Allows for more than one match to the criteria.</td>
   <td>
   ```sparql
    SELECT ?name ?birthDate WHERE {
        VALUES ?name {"Anne" "Mary"}
        ?person cwrc:hasBirthDate ?birthDate.

    }

````
<p>**[This Query](https://yasgui.lincsproject.ca/#)** returns names, these names can either be Anne or Mary.</p>
</td>
</tr>

<tr>
<td>UNION</td>
<td>Allows for grouping of different results, as well as combining multiple graph patterns so that one of several may match</td>
<td>
```sparql
 SELECT ?name ?occupation WHERE {
     ?person rdfs:label ?name.
     {?person cwrc:hasPaidOccupation ?occupation.}
     UNION
     {?person cwrc:hasVolunteerOccupation ?occupation.}
 }
````

   <p>**[This Query](https://yasgui.lincsproject.ca/#)** returns the names and occupations of people who have a volunteer occupation, and people who have paid occupations. By using UNION, the two kinds of occupations are displayed in the same column, rather than needing to have separate columns for each type of occupation.</p>
   </td>
   </tr>
</table>
