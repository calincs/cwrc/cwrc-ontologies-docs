---
sidebar_position: 1
title: "Introduction"
---

# Introduction to the II Ontology

---

## Abstract

The II (Illness and Injury) Ontology is the ontology used by the Canadian Writing Research Collaboratory (**[cwrc.ca](http://www.cwrc.ca/)**) to assign names of illnesses and injuries to cause of death and health issues triples.

## 1. Introduction

This list of Illness and Injury (Morbidity and Mortality) classifications is an abbreviated version of the World Health Organization's (WHO) Startup Mortality List (ICD-10-SMoL). ICD-10-SMoL is the WHO's application of ICD-10 for low-resource settings initial cause of death collection. It was developed to add structure to the designations for cause of death and health issues in biographical information.

## 2. Background

ICD-10-SMoL was chosen because it is a simplified version of the ICD-10. ICD (International Classification of Diseases) "is the foundation for the identification of health trends and statistics globally, and the international standard for reporting diseases and health conditions. It is the diagnostic classification standard for all clinical and research purposes. ICD defines the universe of diseases, disorders, injuries and other related health conditions" [**[http://www.who.int/classifications/icd/en/](http://www.who.int/classifications/icd/en/)**].

Version 1.0 of this ontology was compiled for the CWRC ontology project and reflects the need to have causes of death and health issues organized in a way to allow for analysis of cultural data. A simplified list was deemed sufficient since the domain under study (cultural documents) will not have detailed medical information with regards to cause of death or health issues. But instead of accepting a list of general usage terms for causes of death or health issues that appear in some sources (e.g. Wikipedia, Wikidata, etc.), a standardized, medical classification was selected so that it could be of use for future analysis regarding questions involving the intersection of health and culture.

ICD-10-SMoL was chosen since it was the latest version of this list (June, 2018). A stable version of ICD-11 was to be available in June 2018 but there was no ICD-11-SMoL available at this time. To obtain the ICD10-SMoL: **[http://www.who.int/healthinfo/civil_registration/ICD_10_SMoL.pdf?ua=1](http://www.who.int/healthinfo/civil_registration/ICD_10_SMoL.pdf?ua=1)**

## 3. About this Document

This document is a human-readable version of the ontology that cannot document all of its data structures. The ontology itself should be the primary source for understanding how the ontology works.

The intended audience of this document is the scholar that wishes to understand how the ontology tackles concrete data recording problems and the linked open data practitioner that intends to make use of this ontology.

## 4. Status of this Dynamic Ontology

This document and the associated ontology will grow iteratively with modifications made over time as data is progressively translated and further ontological concerns are identified. Continuity is ensured using the OWL ontology annotations for ontological compatibility and for deprecated classes and properties. Deprecated ontology terms remain present but are marked as such.

The ontology is understood to be a living document that makes no claims to completeness. Instances in particular have been derived from particular datasets and will be expanded progressively over time.

We welcome suggestions for new classes, properties, and predicates from those wishing to use the ontology for their own datasets, as well as suggestions related to the complexity of vocabulary associated with existing terms. Please submit suggestions via an issue or a pull request to the **[CWRC Ontology code repository](https://github.com/cwrc/ontology)**.

## 5. Data Sources

-   **[ICD-10](http://apps.who.int/classifications/icd10/browse/2010/en#/)** [**[Ontology](http://bioportal.bioontology.org/ontologies/ICD10/?p=summary)**]
-   **[SMoL](http://www.who.int/healthinfo/civil_registration/ICD_10_SMoL.pdf?ua=1)**
-   **[Glossary of Medical Terms Used in the 18th and 19th Centuries](https://www.thornber.net/medicine/html/medgloss.html)**

## 6. Basic Ontological Goals

### a. Linkages to External Ontologies

We employ a number of strategies for linking to other ontologies. Our architecture does not typically import other ontologies wholesale, but relates to large vocabularies in defined ways.

We adopt external namespaces and associated classes and terms wherever possible when they are in widespread use and their vocabularies are broadly compatible with ours, as in the case of the **[FOAF](http://xmlns.com/foaf/spec/)** vocabulary. If an external ontology term aligns semantically with ours, then we use OWL- or SKOS-based relationships such as `<owl>:equivalentClass>`, `<skos:narrower>` or `<skos:broader>`. If an external term's definition or use is not commensurate with a term in the CWRC ontology but its application in external datasets is such that it will be useful nevertheless to link those terms to ours, then the **_[has functional relation](/docs/ontologies/cwrc-ontology/cwrc-terms-definitions#hasFunctionalRelation/)_** predicate is employed to indicate that the relationship is specified semantically but may be leveraged for processing.

At the top level, the II ontology makes use of the following well known ontologies:

1. The **[FOAF](http://xmlns.com/foaf/spec/)** ontology for the representation of people and organizations.
2. The **[Web Open Annotation](https://www.w3.org/TR/annotation-model/)** data model is used to link the original Orlando text to specific **_[Contexts](/docs/ontologies/cwrc-ontology/cwrc-terms-definitions#Context)_**.
3. The **[SKOS](https://www.w3.org/TR/skos-reference/)** vocabulary is used to represent taxonomical relationships and to fully document ontology terms.
4. Some **[Dublin Core](http://dublincore.org/documents/dcmi-terms/)** vocabulary terms are used for well known documentation tags.
5. The **[CWRC](/docs/ontologies/cwrc-ontology/cwrc-terms-definitions)** ontology supplies relationships unique to CWRC.

### b. Provenance and Contexts

The **[Web Open Annotation](https://www.w3.org/TR/annotation-model/)** data model is used to link the original Orlando text to specific **_[Contexts](/docs/ontologies/cwrc-ontology/cwrc-terms-definitions#Context)_** for cause of death and health issues. More documentation to come about the way OA was used in this and other CWRC ontologies.

## 7. CWRC Ontology Design Rules

Beyond the formalism of **[The OWL 2 Web Ontology Language](https://www.w3.org/TR/owl-primer/)**, the CWRC ontology follows the following design rules and styles:

-   To be added...

## 8. Notes on SKOS and OWL

SKOS (Simple Knowledge Organization System) enjoys widespread popularity in the semantic web community as it provides simple terms for taxonomies without requiring reasoner support. Whenever appropriate, SKOS terms are inserted within this ontology to link terms to each other. However, since these terms are not ontologically powered, their scalability is limited since each additional layer of terms within a taxonomy requires another database query.

## 9. Conclusion and Future Work

This is a draft ontology that is very much in progress. It will continue to be developed, expanded, and revised as we discover the implications of how we have structured the ontology through using it to extract and explore our data, as fresh data and use cases necessitate expansion or refinement, and as new needs, understandings, and debates arise.

## 10. Version History

-   1.0 - Initial public release. Most of the terms from ICD-10 and SMoL that were deemed appropriate appear in this version but changes to these terms (additions, deletions, modifications) may occur in the future. A new class (Women's Health) appears but is not populated with instances yet. This was added in anticipation of the needs for documenting health issues as opposed to cause of death.

## 11. Bibliography

1. _World Health Organization_. _World Health Organization: Classifications_. [**[Link](http://www.who.int/classifications/icd/en/)**] \

2. _World Health Organization_. _WHO Application of ICD-10 for low-resource settings initial cause of death collection: The Startup Mortality List (ICD-10-SMoL) V2.1_. [**[Link](http://www.who.int/healthinfo/civil_registration/ICD_10_SMoL.pdf?ua=1)**]
