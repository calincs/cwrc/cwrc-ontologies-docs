---
sidebar_position: 1
title: "Introduction"
---

# Introduction to the Genre Ontology

---

## 1. Introduction

The Genre Ontology provides classes and instances for describing the form, mediums, and genres of different types of cultural objects, particularly but not exclusively written ones. It is a pragmatic ontology based on an understanding of genre and related terms as dynamic and  situationally produced in specific rhetorical contexts. Generated from specific datasets, the ontology is designed to be extensible as further instances of genres emerge from specific datasets using the ontology.
In a Linked Open Data (LOD) environment, we welcome the prospect of ongoing expansion, revision, and linking of this ontology to other vocabularies, as the [Canadian Writing Research Collaboratory (CWRC)](https://cwrc.ca/) continues to grow in conversation with the LOD community. 

This introduction accompanies a human-readable version of the ontology (see Terms and Definitions). The ontology itself should be the primary source for understanding how the ontology works.

## 2. Background

Genre, Carolyn R. Miller contends in an influential article, does not inhere in “substance or form of discourse” but must rather be conceived of in terms of situationally-specific rhetorical action, although her definition is more complicated than this brief articulation would suggest, since she argues that “action encompasses both substance and form” (151,152).

Two primary types of action are associated with genre. Miller addresses how particular speech acts or instances of discourse, that is, texts in a broad sense, come themselves to embody generic features; these, she argues, emerge from rhetorical situations which are social constructs by which private intentions are translated into socially recognizable and interpretable discursive acts (Miller 1984). But there is also the act of applying a generic label to a text, another kind of discursive act that operates in a wide range of contexts for different purposes. Generic categorization is foundational to a wide range of disciplines, and most of all to literary and cultural studies, in which generic categorization organizes both primary and secondary materials, defines specializations and literary movements, and plays a major role in the organization and articulation of literary theory, criticism, and history. As a major feature of library classification systems, genre labeling serves research across the disciplines. It organizes bookstores and publishers’ lists, and is used by readers to share and discuss their tastes in texts.

Within these wide-ranging contexts, methods of categorizing specific texts by genre fall into a spectrum ranging from the application by professional cataloguers of tightly controlled authoritative vocabularies produced by “trusted sources” (Harper), to entirely folksonomic categories devised ad hoc by users of online systems--what Clay Shirky has described as “a radical break with previous categorization strategies, rather than an extension of them.” Numerous studies have indicated that while the terms deployed in these various contexts may overlap, they are not consistently applied (Lester; Lu; Rolla).

This is the understanding of genre that informs this ontology. It does not claim to have arrived at a definitive taxonomy of literary forms, media, and genres: such a taxonomy is impossible since these are evolving concepts. Rather, it represents a pragmatic approach to media, form, and genre, very broadly conceived. Its particular emphasis on literary genre emerges from a scholarly history of women's writing. The creators recognize that other languages and approaches will emerge from other contexts, and hope the structure will prove flexible and extensible enough to serve other types of projects. In a Linked Open Data (LOD) environment, various approaches to genre will necessarily intersect, collide, and otherwise interact. We welcome the prospect of ongoing expansion, revision, and linking of this ontology to other vocabularies, as the Canadian Writing Research Collaboratory continues to grow in conversation with the LOD community. 

## 3. Basic Ontological Goals

The genre instances within this ontology are based on the intersection of the genres originally listed by the [Orlando Project](https://orlando.cambridge.org/) with the metadata schema and additional terms required by other projects hosted by the [Canadian Writing Research Collaboratory](https://cwrc.ca/).

Definitions of the genres used have been adopted, adapted, or created so as to provide guidance in the application of the terms. Where possible, these were adapted from the linked open data sets of DBpedia/Wikipedia, the Getty Art & Architecture Thesaurus, or from the *Literary Terms and Definitions* webpage of Dr. L. Kip Wheeler. The indebtedness of the description is indicated using the **provenance ontology**, inline html citations, and within ***`<rdfs:comment>`*** tags, depending on the degree of alignment with the original definition. That said, these definitions can only act as imperfect guides to an understanding of a particular genre, given the complex and situated operation of genre outlined above.

### a. Structure

Some approaches to genre make each term a class that can be applied to an object, whereas others provide instances for each genre. In order to facilitate the ability both to apply terms to particular cultural productions and enable them to be discussed as concepts, genres within the CWRC Genre ontology are instances rather than classes, and are related to particular cultural works through the **property**.

Because it is useful for a number of purposes to be able to move between broader genres and more specific ones, the ontology organizes its terms through a combination of OWL classes that provide broad groupings and **[SKOS](https://www.w3.org/2004/02/skos/)** taxonomical relations amongst instances. The OWL classes provide the ability to reason from narrower to broader terms, while the SKOS properties provide more limited transitive narrower/broader relationships between the instances themselves that can be used to search for relevant works according to those taxonomic relationships amongst instances. One distinctive feature of the ontology is its use of adjectives, as in the class 'Philosophical Genre' and the instance 'detective' to denote types of cultural objects. The classes group multiple genres that fall under that category, while the instance terms can be employed in conjunction with genres that relate more to form, such as 'poem' or 'novel', so as to denote, for instance, 'detective novel'.

The overall structure of the classes is shown here: 

### b. Using this ontology
The terms of this ontology exist as typed instances of either the ***`<Medium>`***, ***`<Form Genre>`***, or ***`<Genre>`*** classes or one of their subclasses. Instances can be accessed directly, via the OWL classes relations, or through the built-in SKOS taxonomical properties. The property ***`<genre:hasGenre>`*** with no ***`<rdfs:domain>`*** permits the easy assignment of a genre to a work without committing to a specific type of creative work.

This ontology can be paired with the **CWRC ontology** or used independently. The CWRC team welcomes suggestions for additions to and refinement of this Genre ontology via its **code repository**.

### c. Translations of definitions

1. Definitions in French, English (and other serendipitously available languages) are never word for word translations and are definitions in their own right, serving to illustrate further the complexity of genre.


## 4. Structure

Some approaches to genre make each term a class that can be applied to an object, whereas others provide instances for each genre. In order to facilitate the ability both to apply terms to particular cultural productions and enable them to be discussed as concepts, genres within the CWRC Genre ontology are instances rather than classes, and are related to particular cultural works through the **[property.](/docs/ontologies/genre-ontology/genre-terms-definitions#hasGenre)**

**[Because it is useful for a number of purposes to be able to move between broader genres and more specific ones, the ontology organizes its terms through a combination of OWL](/docs/ontologies/genre-ontology/genre-terms-definitions#hasGenre)** classes that provide broad groupings and **[SKOS](https://www.w3.org/2004/02/skos/)** taxonomical relations amongst instances. The OWL classes provide the ability to reason from narrower to broader terms, while the SKOS properties provide more limited transitive narrower/broader relationships between the instances themselves that can be used to search for relevant works according to those taxonomic relationships amongst instances. One distinctive feature of the ontology is its use of adjectives, as in the class 'Philosophical Genre' and the instance 'detective' to denote types of cultural objects. The class group multiple genres that fall under that category, while the instance terms can be employed in conjunction with genres that relate more to form, such as 'poem' or 'novel', so as to denote, for instance, 'detective novel'.

The overall structure of the classes is shown here (image produced by **[WebVOWL](http://vowl.visualdataweb.org/webvowl.html)**)

![image.info](/img/genre-structure.png)

## 5. Using this ontology

This ontology was constructed to support the efforts of the CWRC project while enabling its stand-alone use by outside projects. Terms exist as typed instances of either the **_[&lt;Medium>](genre-introduction)_**, **_[&lt;Form Genre>](genre-introduction)_**, or **_[&lt;Genre>](genre-introduction)_** classes or one of their subclasses. Instances can be accessed directly, via the OWL classes relations, or through the built-in SKOS taxonomical properties. The property **[&lt;genre:hasGenre>](/docs/ontologies/genre-ontology/genre-terms-definitions/#hasGenre)** with no **[&lt;rdfs:domain>](https://www.w3.org/TR/owl-ref/#domain-def)** permits the easy assignment of a genre to a work without committing to a specific type of creative work.

This ontology can be paired with the **[CWRC ontology](/docs/ontologies/cwrc-ontology/cwrc-introduction/)** or used independently. The CWRC team welcomes suggestions for additions to and refinement of this Genre ontology via its **[code repository](https://github.com/cwrc/ontology)**.

## 6. Translations of definitions

1. Definitions in French, English (and other serendipitously available languages) are never word for word translations and are definitions in their own right, serving to illustrate further the complexity of genre.

## 7. Future Development

The basic structure of this ontology should be considered stable. Like other CWRC ontologies, this is a living ontology to which changes will be made as new needs and use-cases arise. The ontology will continue to be developed as new projects require additional terms, as it is crosswalked more thoroughly with other vocabularies, and as the CWRC team and related projects explore the implications of a multilingual ontology dedicated to medium, form, and genre. Feedback and suggestions are welcome.

## 8. Version History

-   0.1 - Ontology separated off from main cwrc ontology.
-   0.2 - Definitions and translations added.
-   0.3 - Deprecating of all genre instances and new uris, and instances being typed as Literary Genre.

## 9. Bibliography

Baldick, C. _The Oxford Dictionary Of Literary Terms_. no date. 4th ed., Oxford University Press, 2015. [**[link](https://cwrc.ca/islandora/object/cwrc:4c227091-6bf0-4325-b846-9733fae19e61)**]

Briet, S. _Qu'est-Ce Que La Documentation?_. no date. Éditions Documentaires Industrielles et Techniques, 1951. [**[link](https://cwrc.ca/islandora/object/cwrc:9ee53c63-e387-4fc0-ad17-df5a84dddb4e)**]

Cuddon, J. A. _The Penguin Dictionary Of Literary Terms And Literary Theory_. no date. Edited by C. E. Preston, 4th ed., Penguin Books, 1999. [**[link](https://cwrc.ca/islandora/object/cwrc:2f09376c-f784-4a46-9f04-5a21aa311f27)**]

_Dbpedia_. _Dbpedia_. no date. DBpedia, 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:727d97bc-af78-4122-94be-aa78b2e97d5a)**]

_Dbpedia_. _Dbpedia_. no date. no date. [**[link](https://cwrc.ca/islandora/object/cwrc:c1583789-0dad-41d3-8a42-94d7a8e6d451)**]

Eichhorn, K. ; M. _Prismatic Publics : Innovative Canadian Women's Poetry And Poetics_. no date. Coach House, 2009. [**[link](https://cwrc.ca/islandora/object/cwrc:d4130c87-b761-46d3-b85c-96ed84d3e4f5)**]

_Encyclopædia Britannica_. _Encyclopædia Britannica_. no date. Encyclopædia Britannica, Inc., 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:7ff0c640-f273-46f5-be8d-e50aa27dce59)**]

Foster, J. W. _The Cambridge Companion To The Irish Novel_. no date. Cambridge University Press, 2006. [**[link](https://cwrc.ca/islandora/object/cwrc:076686ab-eab5-4dcf-a6c0-4949d23feb5c)**]

_Getty Art And Architecture Thesaurus_. _Getty Art And Architecture Thesaurus_. no date. The J. Paul Getty Trust, 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:db2f8571-a773-4068-a35f-34262950bb8f)**]

Gruninger, M., and M. Fox. _Methodology For The Design And Evaluation Of Ontologies_. no date. University of Toronto, 1995Apr . [**[link](https://cwrc.ca/islandora/object/cwrc:3f364bc0-ae92-45a1-8716-eaaee1451e91)**]

Harper, C., and B. Tillett. “Library Of Congress Controlled Vocabularies And Their Application To The Semantic Web”. no date. _Cataloging & Classification Quarterly_, 2007. [**[link](https://cwrc.ca/islandora/object/cwrc:13d76949-c3ea-44f9-82c7-4bb58be631f1)**]

_Ifla Study Group On The Functional Requirements For Bibliographic Records_. _Ifla Study Group On The Functional Requirements For Bibliographic Records_. no date. K.G. Saur Verlag, 1998. [**[link](https://cwrc.ca/islandora/object/cwrc:fd6905bf-57dd-4532-9d29-ab4bf0024c1c)**]

Kappeler, S. _The Pornography Of Representation_. no date. University of Minnesota Press, 1986. [**[link](https://cwrc.ca/islandora/object/cwrc:26b39b85-62f3-484c-a2e8-e249b476b927)**]

_L. K. Wheeler Literary Terms_. _L. K. Wheeler Literary Terms_. no date. 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:509015ff-7641-473e-94c5-688e06952bb7)**]

Lester, M. _Coincidence Of User Vocabulary And Library Of Congress Subject Headings: Experiments To Improve Subject Access In Academic Libra_. no date. no date. [**[link](https://cwrc.ca/islandora/object/cwrc:1416a23f-22c3-464e-bd34-d8ba6c435dc6)**]

Lu, C., et al. “User Tags Versus Expert-Assigned Subject Terms: A Comparison Of Librarything Tags And Library Of Congress Subject Headings.”. no date. _Journal Of Information Science_, 2010Nov. . [**[link](https://cwrc.ca/islandora/object/cwrc:c8e2ec75-28a0-41ad-a8a8-1c036f2ce6fa)**]

MazellaMazella, D. D. _The Making Of Modern Cynicism_. no date. University of Virginia Press, 2007. [**[link](https://cwrc.ca/islandora/object/cwrc:93fc3195-0bd5-4161-a123-0352d96c42ab)**]

_Merriam-Webster_. _Merriam-Webster_. no date. Merriam-Webster, Incorporated, 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:0a58baf2-e7d5-47d7-9853-c8140dbbfeed)**]

Michelson, D. “Irreconcilable Differences? Name Authority Control And Humanities Scholarship”. no date. _Hanging Together_, 2013Mar. . [**[link](https://cwrc.ca/islandora/object/cwrc:01be93bb-5c15-4a8c-9bf7-43f42f77047e)**]

Miller, C. “Genre As Social Action”. no date. _Quarterly Journal Of Speech_, 2009June . [**[link](https://cwrc.ca/islandora/object/cwrc:ce03c4db-c840-478c-a656-50f59f398949)**]

Morgan, R. “Theory And Practice: Pornography And Rape”. no date. _Take Back The Night: Women On Pornography_, 1980. [**[link](https://cwrc.ca/islandora/object/cwrc:97cff38b-7268-46fe-b33d-10b0c0623510)**]

Ranganathan, S. R. _Prolegomena To Library Classification_. no date. Asia Publishing House (New York), 1967. [**[link](https://cwrc.ca/islandora/object/cwrc:063341b0-632e-45f7-909a-707a7452d654)**]

Richard, K., and P. Gandel. “The Tower, The Cloud, And Posterity.”. no date. _The Tower And The Cloud_, 2008. [**[link](https://cwrc.ca/islandora/object/cwrc:317f4130-1e38-4671-872a-a487f5e939c1)**]

Stevenson, A. _The Oxford Dictionary Of English_. no date. 3rd ed., Oxford University Press, 2015. [**[link](https://cwrc.ca/islandora/object/cwrc:8bf15f06-bc5f-4619-a374-637bf09aec78)**]

“Advertising Copy”. “Advertising Copy”. no date. _Business Dictionary_, WebFinance Inc., 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:6384921c-fd10-49fa-9828-b20bb55c0fd0)**]
