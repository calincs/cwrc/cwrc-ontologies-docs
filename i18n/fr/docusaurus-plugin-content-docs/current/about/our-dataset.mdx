---
sidebar_position: 3
title: 'Notre ensemble de données'
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Kirisan Suthanthireswaran(@PokeManiac05)
Last Translated: 2024-08-07
description: Ce jeu de données fournit un riche ensemble de données ouvertes liées représentant l'histoire littéraire des femmes depuis les débuts jusqu'à aujourd'hui
---


# L'ensemble de données sur les écrits féminins britanniques d'Orlando Version 1 : Biographie et bibliographie

[01-29-2022] Release 1.52 See **_[1.51 release](#release-151)_** for more details.

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version original.

:::

## Introduction

Ce jeu de données fournit un riche ensemble de données ouvertes liées représentant l'histoire littéraire des femmes depuis les débuts jusqu'à aujourd'hui, concentré sur l'écriture en anglais dans les îles britanniques, mais avec des tentacules vers d'autres langues, traditions littéraires et parties du monde. Il est issu des expériences en cours dans le domaine de l'histoire littéraire menées par le projet Orlando, dont la base de textes est publiée et régulièrement mise à jour et augmentée en tant que **[Orlando: L'écriture féminine dans les îles britanniques, des origines à nos jours](http://orlando.cambridge.org/)** par Cambridge University Press depuis 2006, et du travail du **[Collaboratoire scientifique des écrits du Canada](/docs/about/)**'s en matière de données ouvertes liées (Linked Open Data) comme moyen de faciliter la recherche numérique et la collaboration dans les sciences humaines.

La base de données Orlando Textbase est une collection semi-structurée d'entrées biographiques fournissant des informations détaillées sur la vie et l'écriture de plus de 1400 écrivains, accompagnées de documents littéraires, sociaux et politiques pour fournir un contexte à sa représentation de l'histoire littéraire. Elle ne contient pas de versions numérisées de textes primaires.

L'extraction des données liées de la base de textes d'Orlando a pour but de rendre les données accessibles sous de nouvelles formes pour la découverte, l'interrogation, l'analyse et la visualisation ; de promouvoir l'établissement de liens entre Orlando et d'autres documents connexes sur le web ; et d'expérimenter le potentiel des technologies de données ouvertes liées pour soutenir la production et la diffusion de connaissances dans les sciences humaines.

Il s'agit de la deuxième version de l'ensemble de données, qui est continuellement enrichi et affiné. Cette version se concentre sur la mise en relation interne des informations biographiques à l'aide de l'ontologie du CWRC, avec une mise en relation sélective avec d'autres termes de l'ontologie et d'autres entités de données liées. Toutes les données bibliographiques d'Orlando, liées aux auteurs d'Orlando, sont également incluses dans cette version.

## Questions de Recherche

L'ontologie contient un certain nombre de questions relatives aux compétences. L'ensemble du préambule sera utile pour comprendre l'ontologie et ses utilisations potentielles pour élucider les données. Notez qu'il existe en français et en anglais. Une différence essentielle entre ces données et d'autres ensembles de données est le grand nombre de variables : la consultation de l'ontologie et des ontologies connexes dont les espaces de noms apparaissent en haut de l'ontologie, en relation avec les données, est essentielle à la compréhension de l'ensemble de données.

Dans le cadre de ce concours, nous suggérons quelques questions plus ciblées (voir ci-dessous) que l'ensemble de données actuel est en mesure d'éclairer. Ces questions peuvent être abordées par des visualisations sous différents formats, notamment des graphiques, des graphiques en réseau, des cartes géospatiales, des cartes thermiques, des visualisations de tendances et des infographies.

Exemple d'approche graphique : Pour la question de recherche "Quelle est la pertinence de la taille de la famille par rapport à l'appartenance religieuse, au statut socio-économique ou à d'autres facteurs?", produisez une série de graphiques (en barres, linéaires, circulaires, etc.) qui répondent à cette question. Rendez le processus interactif afin que l'utilisateur puisse sélectionner les facteurs pour une visualisation multidimensionnelle.

Exemple d'approche par graphe de réseau : quels types de réseaux biographiques relient les écrivaines britanniques entre elles et à d'autres écrivains? Quelle est l'étendue des réseaux de parenté par rapport aux réseaux basés sur l'affiliation politique ou religieuse?

Un exemple d'approche cartographique : Pouvez-vous cartographier toutes les informations géographiques relatives à une personne à Orlando? Pouvez-vous également dresser une carte de cette personne et de toutes les personnes avec lesquelles elle est en relation à Orlando? Ces informations cartographiques évoluent-elles au fil du temps, en fonction de l'appartenance ethnique, de la religion, etc. Comment les pays associés à l'héritage géographique évoluent-ils dans le temps?

Exemple d'approche par carte thermique : Si vous prenez l'une des questions de recherche ci-dessous, par exemple "L'identité sociale se diversifie-t-elle avec le temps?", pouvez-vous la représenter sous la forme d'une série de cartes thermiques qui illustrent l'identité sociale (religion, ethnicité, etc.) au fil du temps? Cela pourrait même prendre la forme d'une vidéo telle que **[Anomalies de température par pays 1880-2017](https://www.flickr.com/photos/150411108@N06/43350961005/#)**.

Un exemple d'analyse des tendances : la question de recherche "Quel est le lien entre le nombre de publications et le nombre d'enfants d'une femme écrivain?" pourrait être illustrée à l'aide de graphiques dynamiques, dont on peut voir des exemples dans les vidéos des visualisations Gapminder de Hans Rosling, comme par exemple **[(200 pays, 200 ans, 4 minutes - La joie des statistiques) - BBC Four](https://www.youtube.com/watch?v=jbkSRLYSojo)**.

Un exemple d'approche infographique : Si vous tapez Emily Brontë sur Google, vous obtiendrez une infographie telle que celle présentée ici.

![image.info](/img/EmilyBronte.png)

En utilisant les données d'Orlando, pouvez-vous produire une infographie plus détaillée de l'auteur? Pouvez-vous ensuite l'étendre à d'autres infographies de personnes qui lui sont liées et qui apparaissent également dans les données d'Orlando? Pouvez-vous trouver des informations dans les données d'Orlando et les ontologies qui produiraient une infographie bien différente de celle de Google? À quoi ressemblerait une infographie d'un groupe plus large, par exemple tous les écrivains d'une période historique particulière ou tous les écrivains d'un genre littéraire donné?

### Exemples de questions sur les compétences

1. Quelle est la pertinence de la taille de la famille par rapport à l'appartenance religieuse, au statut socio-économique ou à d'autres facteurs?
2. L'identité sociale se diversifie-t-elle avec le temps? Les formes culturelles hybrides sont-elles plus courantes qu'auparavant? Où se trouvent les groupes d'hybridité et quelles sont les valeurs aberrantes?
3. Que révèlent les visualisations bibliométriques sur les réseaux d'édition dans les données bibliographiques?
4. L'essor et le déclin des différents genres dans les œuvres des auteurs d'Orlando sont-ils en corrélation avec les comptes-rendus des chercheurs sur l'essor et le déclin de ces genres?
5. Quels sont les auteurs les plus isolés dans le réseau, et quels facteurs (par exemple, la classe sociale, le lieu de publication) semblent être associés au fait d'être une aberration?
6. Peut-on établir un lien entre le nombre de publications et le nombre d'enfants d'une femme écrivain?
7. Quels sont les auteurs les plus connectés dans la base de données (le plus grand nombre de points de contact avec d'autres auteurs) et quels sont les types de connexions les plus courants, en termes de relations spécifiques ou de contextes dans lesquels elles se produisent? Les types de connexions recensés dans l'ensemble de données évoluent-ils avec le temps?
8. Que peut révéler une visualisation à la fois sur la structure de l'ontologie et sur les données associées aux différents composants de celle-ci? Par exemple, peut-elle refléter le nombre et les types d'instances associées aux différents composants de l'ontologie, en montrant par exemple les proportions changeantes des différents contextes biographiques tels que la religion, la politique et la sexualité, tels qu'ils apparaissent dans les entrées au fil du temps?

## Linked Open Data

Pour plus d'informations sur le LOD, voir **_[Annexe A](#appendix-a)_**.

## Ensembles de données

### Centré sur le contexte

| Nom | Description | Format | Localisation | Triples |
| --- | --- | --- | --- | --- |
| Biography + Bibliographic Context Centric | Ensemble complet de triples, y compris les ontologies (contient tous les éléments énumérés ci-dessous) | RDF/XML TTL Graph | **http://sparql.cwrc.ca/data/orlando.rdf** [276.7M] **http://sparql.cwrc.ca/data/orlando.ttl** [189.8M] http://sparql.cwrc.ca/data/orlando | 3870906 |
| Biography | La plupart des informations biographiques de l'ensemble de données d'Orlando | RDF/XML TTL Graph | **http://sparql.cwrc.ca/data/orlando/biography.rdf** [105.3M] **http://sparql.cwrc.ca/data/orlando/biography.ttl** [65.3M] http://sparql.cwrc.ca/data/orlando/biography | 1081674 |
| CulturalForms | Un sous-ensemble de biographies axées sur les identités sociales | RDF/XML TTL Graph | **http://sparql.cwrc.ca/data/orlando/culturalforms.rdf** [19.4M] **http://sparql.cwrc.ca/data/orlando/culturalforms.ttl** [11M] http://sparql.cwrc.ca/data/orlando/culturalforms | 192004 |
| Bibliography | Métadonnées bibliographiques normalisées utilisant l'ontologie Bibframe | RDF/XML TTL Graph | **http://sparql.cwrc.ca/data/orlando/bibliography.rdf** [171.4M] **http://sparql.cwrc.ca/data/orlando/bibliography.ttl** [126.3M] http://sparql.cwrc.ca/data/orlando/bibliography | 2240072 |

### Centré sur le sujet (simple)

| Nom | Description | Format | Localisation | Triples |
| --- | --- | --- | --- | --- |
| Biography Simple Triples | Ensemble complet de triples, y compris les ontologies | RDF/XML, TTL, Graph | **http://sparql.cwrc.ca/data/orlando/simple.rdf** [276.7M] **http://sparql.cwrc.ca/data/orlando/simple.ttl** [276.7M] http://sparql.cwrc.ca/data/orlando/simple | 170557 |

### Ontologies

| Nom | Description | Format | Localisation | Triples |
| --- | --- | --- | --- | --- |
| CSÉC | L'ontologie principale | Documentation, RDF/XML , TTL , N-Triples | **http://sparql.cwrc.ca/ontologies/cwrc.html** **http://sparql.cwrc.ca/ontologies/cwrc.rdf** **http://sparql.cwrc.ca/ontologies/cwrc.ttl** **http://sparql.cwrc.ca/ontologies/cwrc.nt** | 17772 |
| L'Ontologie du Genres | Les genres littéraires | Documentation, RDF/XML , TTL , N-Triples | **http://sparql.cwrc.ca/ontologies/genre.html** **http://sparql.cwrc.ca/ontologies/genre.rdf** **http://sparql.cwrc.ca/ontologies/genre.ttl** **http://sparql.cwrc.ca/ontologies/genre.nt** | 8572 |
| L'ontologie II (Illness and Injury) | Classification des maladies et des blessures sur la base de l'IDC | Documentation, RDF/XML , TTL , N-Triples | **http://sparql.cwrc.ca/ontologies/ii.html** **http://sparql.cwrc.ca/ontologies/ii.rdf** **http://sparql.cwrc.ca/ontologies/ii.ttl** **http://sparql.cwrc.ca/ontologies/ii.nt** | 887 |

### Provenance

Les ensembles de données ont été dérivés de la base de textes du projet Orlando, qui "explore et exploite la puissance des outils et des méthodes numériques pour faire progresser les études littéraires féministes" (**[http://www.artsrn.ualberta.ca/orlando/](http://www.artsrn.ualberta.ca/orlando/)**).

L'ensemble de données est basé sur la base de données Orlando, qui comprend 1413 entrées (1117 écrivaines britanniques, 178 écrivains, 177 autres écrivaines - listées deux fois si leur nationalité a changé) ; 13 794 entrées autonomes et 28 807 entrées chronologiques intégrées ; 29 479 listes bibliographiques ; 2 749 854 balises ; 9 038 958 mots (à l'exclusion des balises).

L'ensemble de données Orlando est un texte codé en XML qui utilise plusieurs schémas codant des aspects des entrées numériques du projet et des informations contextuelles sur la vie et l'œuvre des écrivains, qui visent à élucider les conditions de production et de réception de leurs textes, ainsi que les caractéristiques des textes eux-mêmes, dans une perspective critique de récupération qui considère le genre et d'autres forces sociales qui se croisent comme un facteur important de l'histoire littéraire.

La base de texte utilise plusieurs schémas XML :

-   Biographie - structurer les parties des entrées d'Orlando sur la vie des écrivains
-   Rédaction - structurer les parties des entrées d'Orlando sur les carrières et les œuvres littéraires
-   Événement - structurer les événements autonomes
-   Bibliographie - structurer les notices bibliographiques

Le texte encodé avec ces schémas a servi de base au processus d'extraction qui a produit l'ensemble de données fourni ici.

## Extraction et Transformation des données

L'extraction des données a été guidée par les ontologies du CWRC, qui ont été créées sur la base du jeu de balises XML et des données existantes du projet Orlando. Elles s'appuient sur un certain nombre d'ontologies existantes, mais créent également un certain nombre de nouvelles relations et de nombreuses instances de données.

Les ontologies du CWRC sont disponibles à l'adresse suivante **[http://sparql.cwrc.ca/](http://sparql.cwrc.ca/)** à partir desquels ils peuvent être récupérés sous différents formats.

Les ontologies distinctes sont les suivantes: 

-   **[CSÉC ontologie](https://sparql.cwrc.ca/ontologies/cwrc.html)** - L'ontologie principale 
-   **[L'Ontologie du Genres](https://sparql.cwrc.ca/ontologies/genre.html)** - Les genres littéraires
-   **[L'ontologie II (Illness and Injury)](https://sparql.cwrc.ca/ontologies/ii.html)** - Classification des maladies et des blessures sur la base de l'IDC

Le code utilisé pour l'extraction est disponible à l'adresse suivante **[https://github.com/cwrc/RDF-extraction](https://github.com/cwrc/RDF-extraction)**

La méthodologie de base pour produire le RDF à partir du XML d'Orlando consiste à extraire les relations à partir d'emplacements XPath sélectionnés dans le document.

Si des questions très spécifiques subsistent, des informations détaillées sur les décisions d'extraction sont disponibles ici.

## Sous-ensembles de données

Ce projet d'ensemble de données est disponible dans son intégralité ou en trois sous-ensembles :

1. Biographie, contenant :
    1. Naissance et décès - comprend les dates de naissance et de décès des écrivains pour lesquels elles sont connues, avec, dans certains cas, l'ordre de naissance au sein de la famille et la cause du décès ;
    2. Identités culturelles - contient des informations sur les identités sociales associées aux écrivains, allant de la langue, de la religion, de la classe sociale, de la race, de la couleur ou de l'ethnicité à la nationalité. Ces identités évoluent au cours de l'histoire et à certains moments de la vie des écrivains ;
    3. Relations familiales - informations sur les membres de la famille, y compris les conjoints, des écrivains, et parfois des informations relatives à leurs professions ;
    4. Éducation - comprend des liens vers les enseignants, les écoles, les sujets d'étude et les diplômes obtenus ;
    5. Amis - comprend des informations sur les associations informelles, les amitiés étroites et durables et les relations intimes - comprend des informations sur les liens érotiques et non érotiques ;
    6. Loisirs et société - informations sur les activités sociales ;
    7. Professions - couvre à la fois les activités significatives des écrivains et les emplois qu'ils occupent ;
    8. Affiliation politique - informations sur les activités politiques des écrivains, y compris leur affiliation à des groupes ou organisations particuliers et leur degré d'implication ;
    9. Activités spatiales - informations sur les résidences, les visites, les voyages et les migrations des écrivains vers des lieux particuliers. Les coordonnées des données spatiales ne sont granulaires qu'au niveau de l'établissement, c'est-à-dire qu'elles ne font pas de distinction entre les différentes localités d'un même lieu, comme Londres ;
    10. Violence - informations sur les expériences des écrivains en matière de violence à différentes échelles.
    11. Richesse - informations concernant la pauvreté, le revenu et la richesse des écrivains.
    12. Santé - informations sur la santé physique et mentale et les maladies des écrivains ;
    13. Documents biographiques généraux qui ne correspondent pas aux catégories spécifiques.
2. Bibliographie :

    14. Données bibliographiques standard sur les ouvrages publiés par les auteurs dont la vie est décrite dans l'ensemble de données, ainsi que tous les ouvrages référencés dans la base de données Orlando.
    15. Classification des genres pour les textes de femmes écrivains qui ont des entrées dans Orlando.

## Précision et exactitude

Il s'agit d'un ensemble de données "ouvert", ce qui signifie que l'absence d'une affirmation ne signifie pas que cette affirmation est fausse.

Les relations entre les personnes représentées dans cet ensemble de données sont basées sur l'inférence du XML. Les résultats des scripts ont été comparés aux documents clés pour s'assurer que les assertions créées en RDF sont généralement raisonnables, sur la base du balisage, et les scripts ont été ajustés en conséquence. Cependant, tous les résultats n'ont pas pu être vérifiés par des êtres humains, et il est important de se rappeler que les relations XML ont été créées par des êtres humains produisant des comptes rendus discursifs de l'histoire littéraire et cherchant donc à marquer des caractéristiques notables du matériel qu'ils écrivaient, sans savoir que cette extraction aurait lieu plus tard. Il en résulte que cet ensemble de données produira parfois des affirmations trompeuses en fonction de la manière dont les données sont extraites.

Les cas les plus courants sont ceux où une personne mentionnée dans une balise XML utilisée pour créer une relation est impliquée de manière tangentielle plutôt que centrale dans la relation associée à cette balise. Par exemple, la discussion sur le roman Orlando de Virginia Woolf contient une mention du journal intime de Woolf, et cette mention est étiquetée comme un genre ; en conséquence, Orlando est identifié comme un journal intime ainsi que comme une biographie, un drame, une fiction, un roman, une histoire, un masque et une forme de simulacre. Autre exemple, le nom d'un commentateur ou d'un témoin contemporain, s'il est mentionné et étiqueté dans le XML dans la prose d'une balise, peut être extrait et créer une fausse assertion de relation entre le sujet d'une entrée d'Orlando et le commentateur ou le témoin, alors qu'ils étaient nommés dans le document comme source d'information sur la relation. Nous nous sommes efforcés d'éliminer ces fausses affirmations lorsque nous pouvions le faire systématiquement au cours du processus d'extraction des données, et nous prévoyons de mettre au point des moyens plus sophistiqués pour éliminer ces fausses affirmations, par exemple en ce qui concerne les relations personnelles entre des personnes dont les vies ne se sont pas chevauchées.

Toutefois, ces facteurs et d'autres signifient également que toutes les relations indiquées par le XML n'ont pas nécessairement été extraites, en particulier si une telle extraction conduisait à un nombre substantiel de fausses affirmations en plus des vraies. Les scripts d'extraction sont disponibles pour consultation **[ici](https://github.com/cwrc/RDF-extraction)**.

## Omissions

Cet ensemble de données n'est en aucun cas exhaustif, étant donné qu'il est basé à la fois sur des sources historiques pleines de lacunes et sur la sélectivité en ce qui concerne l'inclusion de détails particuliers. Le balisage à partir duquel les données sont extraites est également plus interprétatif, ce qui signifie qu'il y a des incohérences dans l'application, même si les codeurs reçoivent une formation approfondie et que tout le balisage a été revu par un universitaire chevronné.

En outre, il y a des omissions spécifiques dans ce projet d'ensemble de données. Les principaux éléments suivants des données d'Orlando ne sont pas encore présents :

1. Variantes de noms personnels, y compris les pseudonymes ;
2. Des milliers d'événements indépendants qui comprennent des informations sur d'autres écrivains et des contextes historiques ;
3. Les relations littéraires de l'Orlando, autres que celles représentées dans les données bibliographiques ;
4. Les notes scientifiques (pour l'instant uniquement dans la base de données complète d'Orlando).

D'autres omissions sont liées à la provenance de l'ensemble de données et aux priorités du projet Orlando lui-même. Les informations spécifiques sur les genres ne sont présentes que pour les œuvres des écrivaines ayant des entrées et non pour toutes les notices bibliographiques. En général, les informations sur les écrivains masculins et les écrivains non britanniques sont moins complètes que les informations sur les écrivaines britanniques.

Il ne s'agit pas encore d'une  **[5 étoiles LODset](https://www.w3.org/DesignIssues/LinkedData.html)**.  D'autres liens vers d'autres identifiants LOD sont encore à venir, ainsi que des URI déréférençables pour les écrivains eux-mêmes, qui sont actuellement signalés par des identifiants fictifs dans un format basé sur leurs noms standard. Ainsi, bien que **[http://cwrc.ca/cwrcdata/Abdy_Maria](http://cwrc.ca/cwrcdata/Abdy_Maria)** semble être une URL, elle ne se résoudra pas en une page web et l'utilisateur obtiendra une erreur 404. En outre, il existe des "nœuds vides" au format "N59254ff7a0fa44a6b36eb878ea08b7a0" qui apparaissent dans la composante "Annotation Web" des données. Ils sont générés afin de relier chaque annotation/contexte à la chaîne de tortue pour les triples associés à cette annotation, de sorte qu'un système puisse établir ce lien à l'avenir. Dans de nombreux cas, il est préférable de filtrer ces données.

## Décisions et stratégies clés

### Traitement du langage naturel

Le NLP a été utilisé judicieusement pour compléter le XML dans les cas où l'avantage était évident et où l'on pouvait s'attendre à un niveau élevé de précision. Par exemple, le degré élevé de chevauchement du vocabulaire entre les termes utilisés dans la balise `<CAUSEOFDEATH>` et les mots apparaissant dans la balise `<HEALTH>` nous a permis de créer des assertions sur les facteurs de santé qui n'étaient pas pris en charge par le balisage.

### Régularisation, désambiguïsation et mise en relation des données

La régularisation n'était disponible dans le jeu de données que pour les noms de personnes et les organisations. Dans d'autres domaines, tels que les religions, elle a été réalisée par le biais de l'ontologie, dans laquelle la propriété `skos:altLabel` indique les termes qui ont été regroupés.

Dans le cas des noms de lieux, les résultats de l'appariement automatisé (de la combinaison du nom de la localité et de la région et de l'unité géopolitique associées) ont été examinés pour en vérifier l'exactitude, de sorte que les coordonnées géospatiales devraient être exactes pour les lieux jusqu'au niveau de la localité ou de l'endroit habité. Nous utilisons les noms géographiques pour la plupart des identifiants de lieux, complétés par les noms de lieux Getty lorsque cela s'avère nécessaire. Ces correspondances sont faites dans le but de rendre les données cartographiables. Cependant, nous reconnaissons que les changements historiques et la contestation politique peuvent rendre problématiques les étiquettes associées à certains lieux passés et présents.

Bien qu'il soit hautement souhaitable de convertir des chaînes de texte en objets, au sens d'entités définies et déréférençables, cela n'est pas possible ni même souhaitable dans tous les cas, compte tenu de l'état des données sources. Lorsqu'il a été possible d'établir un lien avec une ontologie externe ou de régulariser le vocabulaire au sein des données et de créer des instances LOD de termes au sein de l'ontologie du CWRC, cet ensemble de données l'a fait. Toutefois, dans certains cas, les données étaient tellement idiosyncrasiques ou hétérogènes que cela n'a pas été fait. C'est souvent le cas pour quelques instances au sein d'un sous-ensemble plus large d'instances qui ont été régularisées, comme la religion. Dans le cas de l'éducation, de nombreuses valeurs aberrantes n'ont pas été créées en tant qu'instances de données liées. Il existe des milliers de professions, qui sont actuellement représentées sous forme de chaînes, mais un sous-ensemble de professions communes regroupant les termes est en cours d'élaboration.

## Caractéristiques notables

Les événements fournissent un contenu avec des localisateurs temporels et géospatiaux qui se prêtent à l'établissement de calendriers ou de cartes. Nous nous appuyons sur le modèle d'événement simple, en l'étendant pour indiquer séparément des valeurs de date le degré de certitude associé à la date d'un événement, et en fournissant un typage d'événements spécifique à notre domaine. Tous les contextes n'ont pas encore d'événements, mais tous en auront un jour : pour l'instant, ils se concentrent sur les formes culturelles, la santé, la violence, la richesse, les loisirs et les autres événements de la vie. Bien que les événements de naissance et de décès ne soient pas présents dans cet ensemble de données, il existe des dates de naissance et de décès pour les auteurs qui peuvent être utilisées comme base pour modéliser les changements temporels.

Les contextes utilisent le modèle de données des annotations Web pour fournir des liens entre des entités particulières et leurs contextes discursifs. Les annotations dotées d'un **[oa:identifying](https://www.w3.org/TR/annotation-vocab/#identifying)** motivation indique toutes les entités mentionnées dans un contexte discursif particulier, ce qui permet de regrouper les mentions d'une ou plusieurs personnes par type de contexte. Les annotations dotées d'un **[oa:describing](https://www.w3.org/TR/annotation-vocab/#describing)** motivation inclut comme corps d'annotation l'écrivain qui est décrit.

Les données spatiales sont souvent, mais pas toujours (par exemple, le lieu de décès) régularisées pour **[Geonames](http://www.geonames.org/)** ou le **[Thésaurus Getty des noms géographiques](http://www.getty.edu/research/tools/vocabularies/tgn/index.html)** identifiants.

Les objets bibliographiques utilisent la méthode **[BIBFRAME RDF](http://www.loc.gov/bibframe/docs/index.html)** complété par quelques termes issus de la base de données **[schema.org](https://schema.org/)**.

## Notes de mise à jour

### Version 1.01

01-11-2018

- Supprimé : propriétés invalides sur certains sujets dans les données biographiques, dues à une erreur dans le processus d'extraction.
- Modifié : meilleure conversion des dates invalides dans les données bibliographiques.

### Version 1.51

01-29-2022

- Révision en profondeur du processus d'extraction, y compris de nombreuses mises à jour au cours des quatre dernières années.
- Ceci inclut les données biographiques et bibliographiques.
- Veuillez utiliser les nouvelles versions ci-dessus.

## Licence

Cet ensemble de données est mis à disposition sous une licence CC-BY-NC. Si vous utilisez cet ensemble de données, nous vous remercions de nous en informer à l'adresse **cwrc@ualberta.ca**.

## Les Contributeurs

À la production de cet ensemble de données :

Jeffery Antoniuk. Université de l'Alberta. Programmeur et analyste de systèmes du projet Orlando. Aide à la préparation et à l'extraction des données.

Susan Brown. Université de Guelph. Chef de projet. Production de lignes directrices pour l'extraction et processus guidé **[https://www.uoguelph.ca/~sbrown/](https://www.uoguelph.ca/~sbrown/)**

Joel Cummings. Responsable de la supervision et de la contribution aux travaux techniques sur l'ontologie et de la prise de décisions clés en matière de conception pour soutenir l'extraction à partir d'une variété de sources.

Jasmine Drudge-Willson. Université de Guelph. Assistant de recherche. Responsable de la recherche et de la modélisation des aspects structurels et théoriques des ontologies externes, et de leur relation avec les objectifs de l'ontologie du CWRC.

Hannah Stewart. Université de Guelph. Assistant de recherche. Responsable de l'affinement des définitions des ontologies CWRC/GENRE et du soutien à la modélisation des ontologies.

Abigel Lemak. Université de Guelph. Doctorant en études littéraires. Gestion globale de projets, ainsi que rédaction de termes et de définitions ontologiques, en particulier ceux qui concernent les formes culturelles.

Kim Martin. Université de Guelph. Aide à la gestion du projet. Production d'échantillons de triples pour tester la précision de l'extraction.

Alliyya Mo. Université de Guelph. A rédigé les scripts d'extraction des formes culturelles et une grande partie du reste des données biographiques. Il a créé des données d'instance pour les formes culturelles, les genres, les religions, les affiliations politiques. Responsable également d'une grande partie de l'affinement de l'ontologie reflétant le processus d'extraction.

Michaela Rye. Université de Guelph. Assistant de recherche de premier cycle. Responsable du nettoyage des balises ainsi que de la désambiguïsation et de la rédaction des termes de l'ontologie, en particulier ceux qui traitent des professions.

Gurjap Singh. Université de Guelph. Étudiant coopératif. Responsable de l'extraction initiale des données sur les naissances, les décès et les familles à partir des données d'Orlando. Interrogation de l'API Geonames pour obtenir les URI des lieux d'Orlando.

Thomas Smith. Université de Guelph. Assistant de recherche de premier cycle. Responsable du nettoyage des balises ainsi que de la désambiguïsation et de la rédaction des termes de l'ontologie, en particulier ceux qui traitent des données géospatiales, des prix éducatifs, des prix littéraires et des professions.

Deborah Stacey. Université de Guelph. Professeur associé à l'école d'informatique. A aidé à coordonner le processus.Écriture de scripts pour l'extraction des triples de la cause du décès et de la santé. **[http://ontology.socs.uoguelph.ca/](http://ontology.socs.uoguelph.ca/)**

Voir aussi les crédits du**[Projet Orlando](http://orlando.cambridge.org/public/svDocumentation?formname=t&d_id=CREDITSANDACKNOWLEDGEMENTS)** et les crédits de l'ontologie du CWRC.

## Annexe A

Il existe différents outils pour gérer et produire des données ouvertes liées (LOD) dans leurs différents formats.

CSÉC utilise un **[OWL](https://www.w3.org/OWL/)** **[RDF](https://www.w3.org/TR/rdf-primer/)**, ce qui signifie que les données sont représentées sous forme de triple (sujet, attribut, objet). Le RDF est ensuite exporté vers l'un des différents formats qui expriment le RDF dans une syntaxe unique.

Pour effectuer la transformation et la manipulation de RDF, plusieurs bibliothèques sont disponibles en fonction du langage choisi, voici nos recommandations:

-   Python - **[RDFLib](https://pypi.org/project/rdflib/)**
-   Java - **[Apache Jena](https://jena.apache.org/)**
-   PHP - **[EasyRDF](https://jena.apache.org/)**

Chacune de ces bibliothèques permet de charger les données RDF et de les parcourir sous forme de graphe, ce qui est utile pour identifier les liens entre les données. Des bibliothèques plus complètes comme Apache Jena permettent d'interroger l'ensemble de données au sein de votre application pour déduire et effectuer des requêtes sur les données.

Un ensemble très complet de ressources recommandées sur le LOD a été rassemblé par les gens du projet **[CLAW](https://islandora.ca/CLAW)** d'Islandora, et peut être trouvé **[ici](https://islandora-claw.github.io/CLAW/user-documentation/intro-to-ld-for-claw/)**.

### Interrogation à l'aide du point d'arrivée SPARQL

Nous avons également fourni chaque ontologie dans un graphique afin qu'elle puisse être interrogée à l'aide du **[sparql endpoint](http://sparql.cwrc.ca/sparql)**. Cela nécessite des connaissances en **[SPARQL](https://www.w3.org/TR/rdf-sparql-query/)**, mais c'est un outil puissant d'inspection et de génération.

A titre d'exemple, pour obtenir des informations sur les classes de bibliographie, nous fournissons cette requête que vous pouvez coller dans la boîte SPARQL:

```sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX bf: <http://id.loc.gov/ontologies/bibframe/>

SELECT * WHERE {
  GRAPH <http://sparql.cwrc.ca/db/BibliographyV1> {
      ?sub bf:place ?obj .
    ?obj rdf:value ?o .
  }
}
 LIMIT 10
```
