---
sidebar_position: 1
title: 'Introduction'
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Kirisan Suthanthireswaran(@PokeManiac05)
Last Translated: 2024-08-09
---
# Introduction à l'Ontologie des genres littéraires
---
:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version original.

:::

## Résumé

L'Ontologie des genres littéraires CSÉC est l'ontologie utilisée par le Collaboratoire scientifique des écrits du Canada (cséc.ca) pour assigner des genres à des écrits littéraires.

## 1. Introduction

Une partie des données de bibliographie extensive fournies par les documents du projet Orlando sur les genres littéraires a été rassemblée en web de données ontologique. Son objectif principal était d'annoter les références bibliographiques dans jeu de données avec un vocabulaire adaptable qui pourrait également être utilisé pour la recherche d’œuvres appartenant à un genre spécifique.

## 2. Background

Dans un article influent, Carolyn R. Miller le soutient que le genre littéraire n’est pas inhérent à “une substance ou une forme du discours”, mais doit plutôt être conçu en termes d’activité rhétorique rattachée à une situation spécifique, bien que sa définition soit plus compliquée que cette brève formulation le suggère, puisqu'elle soutient que "l'activité englobe à la fois la substance et la forme" (151, 152).

Deux types d’activités principales sont associées au genre littéraire. Miller s'interroge sur la façon dont certains actes ou cas de discours particuliers, c’est-à-dire les textes au sens large, viennent eux-même incarner des caractéristiques génériques; celles-ci émanent de situations rhétoriques, qui sont des constructions sociales par lesquelles des intentions individuelles sont traduites en actes discursifs socialement reconnaissables et interprétables (Miller, 1984). Mais il existe aussi l’action d’attribuer une balise générique à un texte, ce qui représente un autre type d'acte discursif opérant dans un large panel de contextes et à des fins variées. La catégorisation générique est fondamentale pour les études littéraires, elle organise des matériaux primaires et secondaires, définit des spécialisations et des mouvements littéraires, et joue un rôle majeur dans l'organisation et l’articulation théorique, critique et historique de la littérature. En tant que fonction majeure des systèmes de classification des bibliothèques, l'étiquetage des genres sert à la recherche dans toutes les disciplines. Il sert à organiser les librairies ainsi que les fonds d’édition, et est utilisé par les lecteurs lorsqu’ils échangent leurs expériences de lecture et discutent de leurs préférences littéraires.

Dans une telle variété de contextes, la catégorisation par genre littéraire de textes spécifiques couvre une large spectre de méthodes, allant des catalogues professionnels qui appliquent des vocabulaires d’autorité rigoureusement contrôlés produits par des "sources de confiance" (Harper) aux utilisateurs de systèmes en ligne qui conçoivent ad hoc des catégories entièrement issue de l’indexation personnelle—ce que Clay Shirky a qualifié de “rupture radicale avec les stratégies de catégorisation antérieures, au lieu d’une extension de celles-ci". De nombreuses études ont montré que si des termes correspondants peuvent être déployés dans ces différents contextes, ils ne sont pas systématiquement appliqués (Lester; Lu; Rolla).

C'est une telle interprétation du genre littéraire qui façonne la présente ontolgie. Celle-ci considère qu'une taxonomie définitive des formes littéraires est impossible et ne prétend donc pas élaborer une classification de la sorte. Elle conçoit plutôt les genres littéraires de façon très large et dans une approche pragmatique, tels qu'ils sont apparus au fil des ouvrages académiques sur les femmes de lettres et leurs oeuvres, et tient en compte de la future apparition d'approches formulées dans d'autres langues et provenant de contextes différents. Du fait de son inscription dans un environnement en données ouvertes liées (Linked Open Data), l'ontologie reconnait que diverses approches du genre littéraire seront amenées à se croiser, à se confronter ou à interargir de quelque autre manière, et s'inscrit volontiers dans une perspective d'extension continue, d'expansion ou de liaison avec d'autres vocabulaires pendant que le Collaboratoire Scientifique des Écrits du Canada poursuit son développement au sein avec la communauté LOD.

## 3. Sources de données

La liste des genres utilisés dans cette ontologie s’appuie sur les genres littéraires initialement répertoriés par le projet d'Orlando. La liste est étroitement couplée aux objectifs originaux du projet mais est substantielle.

Comme le projet original d'Orlando n'a pas publié de définitions ou de descriptions des genres qu'il utilisait, ces dernières ont dû être créées spécialement pour cette ontologie. Lorsque cela était possible, les définitions ont été écrites d’après des jeux de données ouverts provenant de DBpedia/Wikipedia, Getty Art & Architecture Thesaurus ou de **_[Termes et définitions littéraires](http://web.cn.edu/kwheeler/lit_terms.html)_** e la page web du Dr. L. Kip Wheeler. Les références aux descriptions sont réalisées grâce à l’utilisation d’ **[ontologie de provenance](https://www.w3.org/TR/prov-o/)**, , de citations html intégrées au texte et à des balises de `<rdfs:commentaires>` tags.

## 4. Structure

Certaines approches du genre font de chaque terme une classe qui peut être appliquée à un objet, tandis que d'autres fournissent des instances pour chaque genre. Afin de faciliter l'application des termes à des productions culturelles particulières et de permettre leur discussion en tant que concepts, les genres de l'ontologie des genres du CCRF sont des instances plutôt que des classes, et sont liés à des œuvres culturelles particulières par le biais de la **[propriété](/docs/ontologies/genre-ontology/genre-terms-definitions#hasGenre)**.

**[Étant donné qu'il est utile, pour un certain nombre d'objectifs, de pouvoir passer de genres plus larges à des genres plus spécifiques, l'ontologie organise ses termes en combinant des classes OWL](/docs/ontologies/genre-ontology/genre-terms-definitions#hasGenre)** qui fournissent des regroupements généraux et des relations taxonomiques **[SKOS](https://www.w3.org/2004/02/skos/)** entre les instances. Les classes OWL permettent de raisonner à partir de termes plus étroits vers des termes plus larges, tandis que les propriétés SKOS fournissent des relations transitives plus limitées entre les instances elles-mêmes, qui peuvent être utilisées pour rechercher des travaux pertinents en fonction de ces relations taxonomiques entre les instances. L'ontologie se distingue par l'utilisation d'adjectifs, comme dans la classe "Genre philosophique" et l'instance "détective", pour désigner des types d'objets culturels. La classe regroupe plusieurs genres qui relèvent de cette catégorie, tandis que les termes de l'instance peuvent être employés en conjonction avec des genres qui se rapportent davantage à la forme, tels que "poème" ou "roman", de manière à désigner, par exemple, un "roman policier".

La structure générale des classes est illustrée ici (image produite par **[WebVOWL](http://vowl.visualdataweb.org/webvowl.html)**)

![image.info](/img/genre-structure.png)

## 5.  Utilization de cette ontologie

Cette ontologie a été construite pour soutenir les efforts du projet CSÉC tout en permettant son utilisation autonome par des projets extérieurs. Les termes existent en tant qu'instances typées des classes **_[&lt;Medium>](genre-introduction)_**, **_[&lt;Form Genre>](genre-introduction)_**, ou **_[&lt;Genre>](genre-introduction)_** u de l'une de leurs sous-classes. Il est possible d'accéder aux instances directement, via les relations entre classes OWL, ou via les propriétés taxonomiques SKOS intégrées. La propriété **[&lt;genre:hasGenre>](/docs/ontologies/genre-ontology/genre-terms-definitions/#hasGenre)** sans **[&lt;rdfs:domain>](https://www.w3.org/TR/owl-ref/#domain-def)** permet d'attribuer facilement un genre à une œuvre sans s'engager sur un type spécifique de travail créatif.

Cette ontologie peut être associée à **[l'ontologie du CSÉC](/docs/ontologies/cwrc-ontology/cwrc-introduction/)** L'équipe du CCRF accueille favorablement les suggestions d'ajouts et d'amélioration de cette ontologie de genre par le biais de son **[dépôt de code](https://github.com/cwrc/ontology)**.

## 6. Règles de conception de l'ontologie Genre

1. Les définitions en Français et Anglais (ainsi bien que les définitions circonstancielles en d'autre langues) ne sont pas des traductions mot à mot et sont des définitions à part entière.

## 7. Conclusion et travail futur

Cette ontologie devra être considérée comme stable par nature, accompagnée d’ajouts ponctuels et de précisions qui seront effectuées de temps à autre. Son développement dépendra de notre découverte progressive de ce qu’implique une ontologie multilingue dédiée aux genre littéraires.

## 8. Historiques des versions

-   0.1 - Ontologie séparée de l’ontologie principale du CSÉC.
-   0.2 - Définitions et traductions ajoutées.
-   0.3 - Dépréciation de toutes les instances de genre et des nouveaux uris, et typage des instances en tant que Genre littéraire.

## 9. Bibliographie

Baldick, C. _The Oxford Dictionary Of Literary Terms_. no date. 4th ed., Oxford University Press, 2015. [**[link](https://cwrc.ca/islandora/object/cwrc:4c227091-6bf0-4325-b846-9733fae19e61)**]

Briet, S. _Qu'est-Ce Que La Documentation?_. no date. Éditions Documentaires Industrielles et Techniques, 1951. [**[link](https://cwrc.ca/islandora/object/cwrc:9ee53c63-e387-4fc0-ad17-df5a84dddb4e)**]

Cuddon, J. A. _The Penguin Dictionary Of Literary Terms And Literary Theory_. no date. Edited by C. E. Preston, 4th ed., Penguin Books, 1999. [**[link](https://cwrc.ca/islandora/object/cwrc:2f09376c-f784-4a46-9f04-5a21aa311f27)**]

_Dbpedia_. _Dbpedia_. no date. DBpedia, 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:727d97bc-af78-4122-94be-aa78b2e97d5a)**]

_Dbpedia_. _Dbpedia_. no date. no date. [**[link](https://cwrc.ca/islandora/object/cwrc:c1583789-0dad-41d3-8a42-94d7a8e6d451)**]

Eichhorn, K. ; M. _Prismatic Publics: Innovative Canadian Women's Poetry And Poetics_. no date. Coach House, 2009. [**[link](https://cwrc.ca/islandora/object/cwrc:d4130c87-b761-46d3-b85c-96ed84d3e4f5)**]

_Encyclopædia Britannica_. _Encyclopædia Britannica_. no date. Encyclopædia Britannica, Inc., 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:7ff0c640-f273-46f5-be8d-e50aa27dce59)**]

Foster, J. W. _The Cambridge Companion To The Irish Novel_. no date. Cambridge University Press, 2006. [**[link](https://cwrc.ca/islandora/object/cwrc:076686ab-eab5-4dcf-a6c0-4949d23feb5c)**]

_Getty Art And Architecture Thesaurus_. _Getty Art And Architecture Thesaurus_. no date. The J. Paul Getty Trust, 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:db2f8571-a773-4068-a35f-34262950bb8f)**]

Gruninger, M., and M. Fox. _Methodology For The Design And Evaluation Of Ontologies_. no date. University of Toronto, 1995Apr . [**[link](https://cwrc.ca/islandora/object/cwrc:3f364bc0-ae92-45a1-8716-eaaee1451e91)**]

Harper, C., and B. Tillett. “Library Of Congress Controlled Vocabularies And Their Application To The Semantic Web”. no date. _Cataloging & Classification Quarterly_, 2007. [**[link](https://cwrc.ca/islandora/object/cwrc:13d76949-c3ea-44f9-82c7-4bb58be631f1)**]

_Ifla Study Group On The Functional Requirements For Bibliographic Records_. _Ifla Study Group On The Functional Requirements For Bibliographic Records_. no date. K.G. Saur Verlag, 1998. [**[link](https://cwrc.ca/islandora/object/cwrc:fd6905bf-57dd-4532-9d29-ab4bf0024c1c)**]

Kappeler, S. _The Pornography Of Representation_. no date. University of Minnesota Press, 1986. [**[link](https://cwrc.ca/islandora/object/cwrc:26b39b85-62f3-484c-a2e8-e249b476b927)**]

_L. K. Wheeler Literary Terms_. _L. K. Wheeler Literary Terms_. no date. 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:509015ff-7641-473e-94c5-688e06952bb7)**]

Lester, M. _Coincidence Of User Vocabulary And Library Of Congress Subject Headings: Experiments To Improve Subject Access In Academic Libra_. no date. no date. [**[link](https://cwrc.ca/islandora/object/cwrc:1416a23f-22c3-464e-bd34-d8ba6c435dc6)**]

Lu, C., et al. “User Tags Versus Expert-Assigned Subject Terms: A Comparison Of Librarything Tags And Library Of Congress Subject Headings.”. no date. _Journal Of Information Science_, 2010Nov. . [**[link](https://cwrc.ca/islandora/object/cwrc:c8e2ec75-28a0-41ad-a8a8-1c036f2ce6fa)**]

MazellaMazella, D. D. _The Making Of Modern Cynicism_. no date. University of Virginia Press, 2007. [**[link](https://cwrc.ca/islandora/object/cwrc:93fc3195-0bd5-4161-a123-0352d96c42ab)**]

_Merriam-Webster_. _Merriam-Webster_. no date. Merriam-Webster, Incorporated, 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:0a58baf2-e7d5-47d7-9853-c8140dbbfeed)**]

Michelson, D. “Irreconcilable Differences? Name Authority Control And Humanities Scholarship”. no date. _Hanging Together_, 2013Mar. . [**[link](https://cwrc.ca/islandora/object/cwrc:01be93bb-5c15-4a8c-9bf7-43f42f77047e)**]

Miller, C. “Genre As Social Action”. no date. _Quarterly Journal Of Speech_, 2009June . [**[link](https://cwrc.ca/islandora/object/cwrc:ce03c4db-c840-478c-a656-50f59f398949)**]

Morgan, R. “Theory And Practice: Pornography And Rape”. no date. _Take Back The Night: Women On Pornography_, 1980. [**[link](https://cwrc.ca/islandora/object/cwrc:97cff38b-7268-46fe-b33d-10b0c0623510)**]

Ranganathan, S. R. _Prolegomena To Library Classification_. no date. Asia Publishing House (New York), 1967. [**[link](https://cwrc.ca/islandora/object/cwrc:063341b0-632e-45f7-909a-707a7452d654)**]

Richard, K., and P. Gandel. “The Tower, The Cloud, And Posterity.”. no date. _The Tower And The Cloud_, 2008. [**[link](https://cwrc.ca/islandora/object/cwrc:317f4130-1e38-4671-872a-a487f5e939c1)**]

Stevenson, A. _The Oxford Dictionary Of English_. no date. 3rd ed., Oxford University Press, 2015. [**[link](https://cwrc.ca/islandora/object/cwrc:8bf15f06-bc5f-4619-a374-637bf09aec78)**]

“Advertising Copy”. “Advertising Copy”. no date. _Business Dictionary_, WebFinance Inc., 2017. [**[link](https://cwrc.ca/islandora/object/cwrc:6384921c-fd10-49fa-9828-b20bb55c0fd0)**]
