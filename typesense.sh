#!/bin/sh

export $(grep -v '^#' .env | xargs)
mkdir -p ./typesense-data

docker run -it -p 8108:8108 \
  -v$PWD/typesense-data:/data typesense/typesense:0.23.0 \
  --data-dir /data --api-key=$TYPESENSE_API_KEY --enable-cors
